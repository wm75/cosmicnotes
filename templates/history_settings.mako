<%!
import posixpath
import json
%>

<%def name="settings(eln_config, page_id)">

<%
from eln import eln_util
from eln import db_modules
    

# define urls and paths
css_url = eln_config.get_url('stylesheets')
images_url = eln_config.get_url('images') 


# Get the projects (sorted by name) that are assigned to this history.
project_ids = db_modules.get_associated_project_ids(
    eln_config.db_engine, [page_id]
    )
assoc_projects = sorted(
    db_modules.get_projects_from_ids(eln_config.db_engine, project_ids),
    key=lambda p: p['name']
    )
# Get the projects (sorted by name), which aren't assigned to this project.
all_projects = db_modules.get_projects_from_ids(eln_config.db_engine)
non_assoc_projects = sorted(
    (p for p in all_projects if p['id'] not in project_ids),
    key=lambda p: p['name']
    )

# build preformatted html select option strings 
assoc_select_options = eln_util.build_select_options(
    (p['name'], trans.security.encode_id(p['id'])) for p in assoc_projects
    )
non_assoc_select_options = eln_util.build_select_options(
    (p['name'], trans.security.encode_id(p['id'])) for p in non_assoc_projects
    )
%>

<!DOCTYPE HTML>
<html>
  <head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- load css file  -->
    ${h.stylesheet_link(posixpath.join(css_url, 'eln.css'))}
    ${h.stylesheet_link(posixpath.join(css_url, 'CKeditor_content.css'))}
    <style>
        body { 
        background-color: rgba(244,244,244,0.79);
        }
    </style>
    <!-- include Galaxy's copy of jquery -->
    ${h.js('libs/jquery/jquery')}
  </head>
  
  <body>
    <div class='page_eln_header'>
        <div class='page_header_logo_box'>
            <img src="${posixpath.join(images_url, 'pen.png')}" />
        </div>    
        <div class='page_header_title_box'>
        Electronic Lab Notebook, History Metadata 
        </div>
        <div class='page_header_action_box'>    
            <form action="${eln_config.get_url('show')}" method="get" id="go_back">
                <input type="submit" value="Back" class="button">
                <input type="hidden" name="dataset_id" id="dataset_id" value="${trans.security.encode_id(hda.id)}" class="button">
            </form>  
        </div>
    </div> 
    <div class='page_body'>
        <p class="seperator">Project Management</p>
        <div class="toolForm">
            <div class="toolFormBody">
                <form id="associate_page_project" name="associate_page_project" action="${eln_config.get_url('show')}" method="get">
                    <div class="form-row">
                        <div style="float: left; margin-right: 10px;">
                            <label>Projects associated with current page</label>
                            <br>
                            <select id="assoc_with_list" name="assoc_with_list" style="min-width: 250px; height: 150px;" multiple="">
                                ${'\n'.join(assoc_select_options)}
                            </select>
                            <br>
                            <input id="project_disassoc_button" value=">>" type="submit">
                        </div>
                        <div>
                            <label>Projects not associated with current page</label>
                            <br>
                            <select id="non_assoc_with_list" name="non_assoc_with_list" style="min-width: 250px; height: 150px;" multiple="">
                                ${'\n'.join(non_assoc_select_options)}
                            </select>
                            <br>
                            <input id="project_assoc_button" value="<<" type="submit">
                        </div>
                    </div>
                    <div class="form-row">
                        <input id="new_project_name" value="" type="text">
                        <input id="add_new_project_button" value="Create New Project" type="button">
                    </div>
                    <div class="form-row">
                        <input id="page_project_assoc_save_button" value="Save" type="button">
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <script>
        var projects_to_associate = [],
            projects_to_disassociate = [],
            encoded_hist_id = '${trans.security.encode_id(page_id)}',
            show_baseurl = '${eln_config.get_url('show')}';


        function AddAssociations(project_ids) { 
            var url = show_baseurl;
            request = {request_type: 'update_projects',
                       params: {action: 'insert_association',
                                page_id: encoded_hist_id}
                      }
            // TO DO: make this a single request instead of one per item
            array_length = project_ids.length;
            for (var i = 0; i < array_length; i++) {
                request['params']['project_id'] = project_ids[i];

                $.ajax( {
                    url: url,
                    async: false,
                    type:  'POST',
                    data:  {
                            eln_request: request.request_type,
                            eln_request_params: JSON.stringify(request.params),
                            eln_request_data: ''
                           } 
                });
            }                
        }


        function RemoveAssociations(project_ids) { 
            var url = show_baseurl;
            request = {request_type: 'update_projects',
                       params: {action: 'delete_association',
                                page_id: encoded_hist_id}
                      }
            // TO DO: make this a single request instead of one per item
            array_length = project_ids.length;
            for (var i = 0; i < array_length; i++) {
                request['params']['project_id'] = project_ids[i];

                $.ajax( {
                    url: url,
                    async: false,
                    type:  'POST',
                    data:  {
                            eln_request: request.request_type,
                            eln_request_params: JSON.stringify(request.params),
                            eln_request_data: ''
                           } 
                });
            }                
        }
            
        function NewProject(new_project_name) {                 
            var url = show_baseurl;
            request = {request_type: 'update_projects',
                       params: {action: 'insert_project',
                                project_name: new_project_name}
                      }
            req = $.ajax( {
                url: url,
                type:  'POST',
                data:  {
                        eln_request: request.request_type,
                        eln_request_params: JSON.stringify(request.params),
                        eln_request_data: ''
                       } 
            });
            return req; 
        }


        // set up event listeners
        // When the disassociate project button gets clicked,
        // the selected associated projects get removed from the associated
        // projects select box and are added to the non-associated projects
        // select box instead.
        // To keep track of changes since the last Save, the
        // projects_to_associate and projects_to_disassociate arrays
        // are updated: if a project has been associated since the last Save,
        // this is reversed; other projects are marked as needing disassociation.
        $('#project_disassoc_button').click( function() {
          $('#assoc_with_list option:selected').each( function() {
              assoc_index = jQuery.inArray(this.value, projects_to_associate);
              if (assoc_index > -1) {
                  projects_to_associate.splice(assoc_index, 1);
              }
              else {
                  projects_to_disassociate.push(this.value);
              }
          } );
          return !$('#assoc_with_list option:selected')
                  .remove().appendTo('#non_assoc_with_list');
        } );
        // The associate project button needs the inverse of the above logic.
        $('#project_assoc_button').click( function() {
          $('#non_assoc_with_list option:selected').each( function() {
              disassoc_index = jQuery.inArray(this.value, projects_to_disassociate);
              if (disassoc_index > -1) {
                  projects_to_disassociate.splice(disassoc_index, 1);
              }
              else {
                  projects_to_associate.push(this.value);
              }
          } );
          return !$('#non_assoc_with_list option:selected')
                  .remove().appendTo('#assoc_with_list');
        } );
        // When the Save button gets clicked, changes since the last Save
        // (kept track of by the projects_to_associate and
        // projects_to_disassociate arrays are stored and the two tracking
        // arrays are reset.
        $('#page_project_assoc_save_button').click( function() {
            AddAssociations(projects_to_associate);
            projects_to_associate = [];
            RemoveAssociations(projects_to_disassociate);
            projects_to_disassociate = [];
        } );
        // When the New Project button get clicked and a project name has been
        // entered, an attempt is made to create a new project with that name
        // on the server. If successful, this request returns the encoded
        // id of the new project. It returns null if the name is taken.
        // The new project is added to the associated projects select box and
        // the association is marked as pending until the next Save operation.
        $('#add_new_project_button').click( function() {
            if ($('#new_project_name').val()) {
                project_name = $('#new_project_name').val()
                new_project_request = NewProject(project_name)
                new_project_request.done(function (response){
                    console.log(response);
                    response = JSON.parse(response);
                    console.log(response);
                    if (response === null) {
                        alert('A project with this name exists already.');
                    }
                    else {
                        projects_to_associate.push(response);
                        $('#assoc_with_list').append(
                            $('<option/>', {value: response, text: project_name})
                            );
                        $('#new_project_name').val('')
                    }
                });
            }
        } );
    </script> 
  </body>
</html>
</%def>
