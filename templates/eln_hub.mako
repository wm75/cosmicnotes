<%!
import os
import sys
import json
import datetime
%>

<%
# append path to eln-specific modules to sys.path
this_file = os.path.normpath(os.path.join(util.galaxy_root_path, self.filename))
app_dir = os.path.dirname(os.path.dirname(this_file))
lib_path = os.path.join(app_dir, 'static/lib')

if lib_path not in sys.path:
    sys.path.append(lib_path)


import eln.requests

from eln import eln_util
from eln import experiment


root = h.url_for( "/" )
eln_config = eln_util.ElnConfig(app_dir, root)


if trans.request.params.has_key('eln_request'):
   eln_request = {
       'request_type': trans.request.params['eln_request'],
       'params': json.loads(trans.request.params['eln_request_params']),
       'data': trans.request.params['eln_request_data']
       }
   if 'page_id' in eln_request['params']:
       # Page IDs are Galaxy history IDs and always passed in encoded form
       # between client and server. Here on the server side, we decode them so
       # that our Python library can work with them.
       eln_request['params']['page_id'] = trans.security.decode_id(
           eln_request['params']['page_id']
           )
   if 'project_id' in eln_request['params']:
       # Project IDs are passed in encoded form between client and server.
       # Here on the server side, we decode them so
       # that our Python library can work with them.
       eln_request['params']['project_id'] = trans.security.decode_id(
           eln_request['params']['project_id']
           )
   response = eln.requests.handle_request(eln_config, eln_request)
   if isinstance(response, experiment.ExpID):
       # This is an object id (page, user or project) that needs to be
       # encoded.
       response = trans.security.encode_id(response)
   elif response is not None:
       # Assume this is a dict in which keys requiring encoding are instances
       # of experiment.ExpID.
       # To make the response JSON serializable instances of datetime.datetime
       # need to be converted to str.
       for key in response:
           if isinstance(response[key], experiment.ExpID):
               response[key] = trans.security.encode_id(response[key])
           elif isinstance(response[key], datetime.datetime):
               response[key] = str(response[key])
%>


%if trans.request.params.has_key('eln_request'):
    ${json.dumps(response)}
%elif trans.request.params.has_key('overview'):
    <%namespace name="overview" file="overview.mako"/>
    ${overview.overview(trans, eln_config)}
%elif trans.request.params.has_key('overview_histdict'):
    <%namespace name="respond" file="overview_respond.mako"/>
    ${respond.overview_response(trans, eln_config)}
%elif trans.request.params.has_key('show_history'):
    <%namespace name="show_hist" file="show_history.mako"/>
    ${show_hist.show_history(
          eln_config,
          trans.request.params['show_history'],
          int(trans.request.params['section_no']),
          trans.request.params['version_a'] or None,
          trans.request.params['version_b'] or None
          )}
%elif trans.request.params.has_key('settings'):
    <%namespace name="settings" file="history_settings.mako"/>
    ${settings.settings(
          eln_config, trans.security.decode_id(trans.request.params['settings'])
          )}
%else:
    <%namespace name="editor" file="editor.mako"/>
    ${editor.edit(eln_config)}
%endif


