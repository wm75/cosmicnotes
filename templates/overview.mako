<%def name="overview(trans, eln_config)">
<%
import posixpath


root = h.url_for("/")
css_url = eln_config.get_url('stylesheets')
images_url = eln_config.get_url('images')

    
kwargs = { 'app' : 'app' }

%>
    
    ${h.css(
        'base',
        'jquery.rating'
    )}
    
    ${h.js(
        'libs/jquery/jquery',
        'libs/jquery/jquery-ui',
        'libs/require',
        'libs/underscore',
        'bundled/libs.bundled',
    )}

<!DOCTYPE HTML>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
       <!-- load css file  -->
    ${h.stylesheet_link(posixpath.join(css_url, 'eln.css'))}
    ${h.stylesheet_link(posixpath.join(css_url, 'jquery-ui.css'))}   
    ${h.stylesheet_link(posixpath.join(css_url, 'overview_mask.css'))}

    </head>
        <body>
        <script type="text/javascript">
        var search_project = '',
            search_author = '',
            text_search = '',
            active_project_query = search_project,
            active_author_query = search_author,
            active_text_query = text_search,
            pages = [],
            author_tags = [],
            project_tags = [],
            project_id_mapping = {},
            order = 'update_time',
            sub_filter = 'both',
            show_baseurl = '${eln_config.get_url('show')}';


        // ---------------- Galaxy code ---------------------- 
        
        // required for following scripts
        var galaxy_config = {root : '${root}'};
          
        // path for requirejs, uses now galaxy scripts.
        // Two addresses are served from Galaxy server. The default script
        // address and the visualisations directory
           
        require.config({
            baseUrl:  "/static/scripts",
            paths: { config: "/${eln_config.get('URLPaths', 'static')}" },
            shim: {
                "libs/underscore": {exports: "_"},
                "libs/backbone": {
                    deps: [ 'jquery', 'libs/underscore' ],
                    exports: "Backbone"
                }
            },
            map: {
                'mvc/tool/tools': {
                    'templates/tool_form.handlebars'    : 'templates/compiled/tool_form',
                    'templates/tool_search.handlebars'  : 'templates/compiled/tool_search',
                    'templates/panel_section.handlebars': 'templates/compiled/panel_section',
                    'templates/tool_link.handlebars'    : 'templates/compiled/tool_link',
                },
            },
        });
    
        // update the window.bootstrapped variable, necessary for following galaxy scripts
        %for key in kwargs:
            ( window.bootstrapped = window.bootstrapped || {} )[ '${key}' ] = (
                ${ h.dumps( kwargs[ key ], indent=( 2 if trans.debug else 0 ) ) } );
        %endfor
        define( 'bootstrapped-data', function(){
            return window.bootstrapped;
        });
    
        // creates a new Galaxy object, necessary for following galaxy scripts
        require([ 'require', 'galaxy' ], function( require, galaxy ){
            window.Galaxy = new galaxy.GalaxyApp({
                root            : galaxy_config.root,
                loggerOptions   : {}
            });
        });    
    
        define( 'jquery', [], function(){ return jQuery; });
            

        function UpdatePanelView(){
        // Update the multipanel view to contain the current set of filtered pages.
            active_project_query = search_project = $('#project_input').val() || '';
            active_author_query = search_author = $('#author_input').val() || '';
            active_text_query = text_search = $('#text_search_input').val() || '';

            var user_histories = [];
            var shared_histories = [];
            var published_histories = [];

            function get_histories () {
                var df = jQuery.Deferred();

                var user_historiesXHR = jQuery.ajax(
                    '/api/histories?keys=id,name,user_id'
                );
                user_historiesXHR.done( function(response_histories){
                  user_histories = response_histories;
                  df.notify({status: 'user histories retrieved'});
                });
                user_historiesXHR.fail( function( xhr, status, message ){
                  df.reject( xhr, 'loading user histories');
                });

                var shared_historiesXHR = user_historiesXHR.then( function(){
                  return jQuery.ajax(
                    '/api/histories/shared_with_me?keys=id,name,user_id'
                  );
                });
                shared_historiesXHR.done( function(response_histories){
                  shared_histories = response_histories;
                  df.notify({status: 'shared histories retrieved'});
                });
                shared_historiesXHR.fail( function( xhr, status, message ){
                  df.reject( xhr, 'loading shared histories');
                });

                var published_historiesXHR = shared_historiesXHR.then( function (){
                  return jQuery.ajax(
                    '/api/histories/published?keys=id,name,user_id'
                  );
                });
                published_historiesXHR.done( function(response_histories){
                  published_histories = response_histories;
                  df.notify({status: 'published histories retrieved'});
                });
                published_historiesXHR.fail( function( xhr, status, message ){
                  df.reject( xhr, 'published user histories');
                });
                published_historiesXHR.then( function(){
                  df.resolve(user_histories.concat(shared_histories, published_histories));
                });

                return df;
            }

            function get_pages (combined_histories){
                request = jQuery.ajax( {
                    url: show_baseurl,
                    type: 'POST',
                    data: {
                        author: search_author,
                        project: project_id_mapping[search_project],
                        sub_filter: sub_filter,
                        text_search: text_search,
                        overview_histdict: JSON.stringify(combined_histories)
                        }
                });
                return request;
            }

            function RenderColumns (pages, order) {
            // calls Galaxy scripts to create and render the multiple view page
                require([
                    'config/history-model',
                    'mvc/history/multi-panel'
                ], function( HISTORY_MODEL, MULTI_PANEL ){
                    $(function(){
                        histories = new HISTORY_MODEL.HistoryCollection(pages, {
                            includeDeleted  : true,
                            order           : order,
                            allFetched      : true
                            // Deleted current history entry to remove its behaviour
                            // in the sort functionallity
                        });
                        multipanel = new MULTI_PANEL.MultiPanelColumns({
                            el                          : $( '#center' ).get(0),
                            histories                   : histories
                        }).render( 0 );
                    });
                });
            }

            histories_request = get_histories();
            histories_request.done(function (combined_histories){
                pages_request = get_pages(combined_histories);
                pages_request.done(function (response){
                    response = JSON.parse(response)
                    pages = response['pages'];
                    author_tags = response['authors'];
                    project_tags = Object.keys(response['projects']).sort();
                    project_id_mapping = response['projects'];
                });
                pages_request.done(function (){
                    RenderColumns(pages, order);
                    // Refresh modal project list
                    $('#select_project_list').empty()
                    $.each(project_tags, function(key, value) {
                         $('#select_project_list')
                             .append($("<option></option>")
                             .attr("value", key)
                             .text(value));
                    });
                });
            });
        }

        UpdatePanelView();
                 

        // ------------- Page masking -----------------------
          
        function PageMask(){
            AddInputBoxes();
            var pages_no = pages.length;
            var page;
            for (var n = 0; n < pages_no; n++){
                page = pages[n];
                var column_obj_name = "#history-" + page.id;
                var eln_button_name = "#eln-" + page.id;
                // Checks if history-column exists
                if ($(column_obj_name).length > 0) {
                    var controls_element = $(column_obj_name).find(".controls");
                    // Checks if eln button already exists.
                    if (!$(eln_button_name).length > 0) {
                        var actions_element = controls_element.find(".actions"),
                            datasets = $(column_obj_name).find(".list-items").children();
                        if (datasets.length > 0) {
                            dataset_id = datasets[0].id.substring(8);
                            //Append button to the history column.
                            var eln_button = CreateElnBtn(page, dataset_id);
                            actions_element.append(eln_button);
                            // Color button if search hit.
                            if (page.fulltext_search_hit){
                                eln_button.setAttribute('style', 'color:green');
                            }
                        }
                    }
                    if (!$('#eln_info-' + page.id).length > 0) {
                        eln_info = InfoGenerator(
                            page.project_list, page.username,
                            page.update_time.substring(0,10)
                        );
                        eln_info.attr('id', 'eln_info-' + page.id);
                        controls_element.append(eln_info);
                        //find(".history-size")
                        SetupTextListeners();
                        if (page.submitted) {
                            var lock_icon = CreateLockIcon();
                            actions_element.append(lock_icon);
                        }
                    }
                }
            }
        }


        function CreateElnBtn (page, dataset_id) {
            var new_form = document.createElement("a");
                new_form.setAttribute('href', show_baseurl +
                                              '?dataset_id=' + dataset_id);
                new_form.setAttribute('id',"eln-" + page.id);
                new_form.setAttribute('class',"icon-btn display-btn");
                new_form.setAttribute('title',"Go to the electronic lab notebook.");         
    
            var icon = document.createElement("span");
                icon.setAttribute('class',"fa fa-edit");
    
            new_form.appendChild(icon);
            return new_form;
        }


        function CreateLockIcon () {
            var new_form = document.createElement("a");
                new_form.setAttribute('class',"icon-btn display-btn");
                new_form.setAttribute('title',"This eln is already submitted.");
            
            var icon = document.createElement("span");
                icon.setAttribute('class',"fa fa-lock");
            
            new_form.appendChild(icon);
            return new_form;
        }
        

        function InfoGenerator (project_list, author, update_time) {
            // Returns an HTML string accordingly to the number of projects
            // assigned to a history.
            
            append_projects = function (container, how_many) {
                var project, project_link;
                for (i = 0; i < how_many; i++) {
                    project = $('<p />');
                    project.attr('class', 'project');
                    project_link = $('<a />');
                    project_link.text(project_list[i]);
                    project.append(project_link);
                    container.append(project);
                }
            };
            var info_element = $('<div />');
            info_element.attr('class', 'ELNinfo');
            var author_info = $('<a />');
            author_info.attr('class', 'author');
            author_info.text(author);
            
            var update_info = $('<p />');
            update_info.text(update_time);
            
            var projects_header = $('<div />');
            projects_header.attr('style', 'float-left');
            projects_header.text('Projects:');
            
            var projects_container = $('<div />');
            projects_no = project_list.length;
            if (projects_no == 0) {
                projects_container.html(
                    '<font color="red"><i>no project assigned</i></font>');
            }
            else {
                if (projects_no < 5) {
                    append_projects(projects_container, projects_no);
                }
                else {
                    append_projects(projects_container, 3);
                    ao = $('<span />');
                    ao.attr('class', 'span_mouse');
                    ao.text('...');
                    ao.attr('title', project_list.join(';'));
                    projects_container.append(ao);
                }
            }
            info_element.append(author_info);
            info_element.append(update_info);
            info_element.append(projects_header);
            info_element.append(projects_container);
            return info_element
        }
        

        function AddInputBoxes() {
            
            function ConstructSearchField (search_request) {
                var outer_div = document.createElement("div");
                outer_div.setAttribute('class', "search-control search-input");
                                    
                var new_input = document.createElement("input");
                new_input.setAttribute('id', search_request+"_input");   
                new_input.setAttribute('type', "text"); 
                new_input.setAttribute('placeholder', "search "+search_request);   
                new_input.setAttribute('class', "search-query");               
                
                new_input.setAttribute('data-toggle', "popover_"+search_request);
                new_input.setAttribute('data-trigger', "manual");
                new_input.setAttribute('data-content', "This "+search_request+" doesn't exists.");
                new_input.setAttribute('data-placement', "left");
                
                var new_span = document.createElement("span");
                new_span.setAttribute('class', "search-clear fa fa-times-circle");
                new_span.setAttribute('id', "clear_"+search_request+"_search");
                
                outer_div.appendChild(new_input);
                outer_div.appendChild(new_span);
                
                $('.control-column.control-column-right.flex-column').append(outer_div);
            }
            
            function CreateProjectListIcon () {
                var new_form = document.createElement("a");   
                    new_form.setAttribute('id',"list_projects_btn");
                    new_form.setAttribute('class',"icon-btn display-btn");
                    new_form.setAttribute('title',"Show all projects");         
        
                var icon = document.createElement("span");
                    icon.setAttribute('class',"fa fa-list");
        
                new_form.appendChild(icon);
                $('.control-column.control-column-right.flex-column').append(new_form); 
            }     
            
            function CreateTextSearchIcon () {
                var new_form = document.createElement("a");   
                    new_form.setAttribute('id',"text_search_btn");
                    new_form.setAttribute('class',"icon-btn display-btn");
                    new_form.setAttribute('title',"Text Search");         
        
                var icon = document.createElement("span");
                    icon.setAttribute('class',"fa fa-search");
        
                new_form.appendChild(icon);
                $('.control-column.control-column-right.flex-column').append(new_form); 
            }     
            
            if (!$("#author_input").length > 0) {   
                // Disable fulltext searches for the moment
                // %if eln_config.db_is_postgres() or eln_config.db_is_mysql():
                //     CreateTextSearchIcon();
                // %endif
            
                ConstructSearchField('author');
                ConstructSearchField('project');
                if (search_author != '') {
                    $('#author_input').val(search_author);
                }
                if (search_project != '') {
                    $('#project_input').val(search_project);
                }
                
                // Disable fulltext search for now.
                // Populate text search modal
                // $('#text_search_input').val(text_search);
                // open text-search modal if there was a search query and no hit was found.
                // if ($.isEmptyObject(lb_search_hits) && text_search != '') {
                //    document.getElementById("text_search_modal_header")
                //        .innerHTML = 'Your search request was not found. ' +
                //                     '<span class="close" ' +
                //                     'id="close_text_search_modal">X</span>';
                //    $( "#modal_text_search" ).show();
                // }

                CreateProjectListIcon();                
                SetupSearchListeners();
            }
        }
                     

        function SetupSearchListeners() {          
            // Popover
            $('[data-toggle="popover_author"]').popover({delay: {show: 100, hide: 2000}})
            $('[data-toggle="popover_project"]').popover({delay: {show: 100, hide: 2000}})
            
            // Autocomplete
            $("#author_input").autocomplete({
                source: author_tags
            });
            $("#project_input").autocomplete({
                source: project_tags
            });
        
            // Pressing enter 
            $("#author_input").keypress(function (e) {
                if (e.which == 13) {
                    if (!($('#author_input').val()) ||
                        _.contains(author_tags,$('#author_input').val())) {
                        if ($('#author_input').val() != active_author_query){
                            UpdatePanelView();
                        }
                    }
                    else{
                        $("[data-toggle='popover_author']").popover('show');                    
                        $("[data-toggle='popover_author']").popover('toggle');  
                    }
                return false;
                }
            });
            $("#project_input").keypress(function (e) {
                if (e.which == 13) {
                    if (!($('#project_input').val()) || 
                      _.contains(project_tags,$('#project_input').val())) {
                      if ($('#project_input').val() != active_project_query){
                        UpdatePanelView();
                      }
                    }
                    else{
                        $("[data-toggle='popover_project']").popover('show');                    
                        $("[data-toggle='popover_project']").popover('toggle');  
                    }
                return false;
              }
            });
            $("#text_search_input").keypress(function (e){  
                if (e.which == 13) {
                    if ($('#text_search_input').val() != active_text_query){
                        UpdatePanelView();
                    }
                }
            });
            
            // Click on x
            $( "#clear_project_search" ).click(function(){
                if ($('#project_input').val()) {
                    $('#project_input').val('');
                    if ($('#project_input').val() != active_project_query){
                      UpdatePanelView();
                    }
                }
            });
            $( "#clear_author_search" ).click(function(){
                if ($('#author_input').val()) {
                    $('#author_input').val('');
                    if ($('#author_input').val() != active_author_query){
                        UpdatePanelView();
                    }
                }
            });  
            
            // Project list modal Listeners            
            $( "#list_projects_btn" ).click(function(){
                $( "#modal_project_list" ).show();      
                
            }); 
            $( "#close_modal" ).click(function(){
                $( "#modal_project_list" ).hide();
            }); 
                        
            $( "#select_project_from_list_btn" ).click(function(){
                var select_project_list = document.getElementById("select_project_list");
                if(select_project_list.options[select_project_list.selectedIndex] == undefined){ 
                    // TODO: Stop UpdatePanelView and set for example first option as selected.
                    UpdatePanelView();
                }
                else {
                    var selection = $('#select_project_list option:selected').text();
                    $('#project_input').val(selection);
                    if ($('#project_input').val() != active_project_query){
                      UpdatePanelView();
                    }
                }
                $( "#modal_project_list" ).hide();
            });     
            
            $("option").bind("dblclick", function(){
                var selection = $('#select_project_list option:selected').text();
                $('#project_input').val(selection);
                $( "#modal_project_list" ).hide();
                if ($('#project_input').val() != active_project_query){
                    UpdatePanelView();
                }
            });         
                        
            // Text search modal Listeners            
            $( "#text_search_btn" ).click(function(){
                $( "#modal_text_search" ).show();
                $("#text_search_input").focus();
            }); 
            $( "#close_text_search_modal" ).click(function(){
                $( "#modal_text_search" ).hide();
            });             

            $("#search_txt_btn").click(function(){
                if ($('#text_search_input').val() != active_text_query){
                    UpdatePanelView();
                }
            });

        }  
        

        function SetupTextListeners () {
            SaveOrder ();
            // For unknown reasons the normal html tooltip isn't
            // correctly running on the page. As a replacement the
            // JQuery-ui plugin tooltip is used. However the tooltip
            // modal won't close when the mouse leaves the element.
            // Therefor an own mouseout listener was written.
            $( ".span_mouse" ).tooltip();
            $( ".span_mouse" ).mouseout(function() {
                $( ".tooltip.fade.top.in" ).remove();
            });     
            
            $( ".project" ).click(function(){
                $('#project_input').val($(this).text());
                if ($('#project_input').val() != active_project_query){
                    UpdatePanelView();
                }
            });
            
            $( ".author" ).click(function(){
                $('#author_input').val($(this).text());
                if ($('#author_input').val() != active_author_query){
                    UpdatePanelView();
                }
            });
        }
        
        
        function SaveOrder () {
            // Setup a Click listener to the dropdown options.
            // On Click the global 'order' variable  gets assigned to the choosen
            // dropdown option.

            function SetOrder (selection) {
                if (selection == 'most recent first'){
                    order = 'update_time';}   
                else if (selection == 'least recent first'){
                    order = 'update_time-asc';}
                else if (selection == 'name, a to z'){
                    order = 'name';}
                else if (selection == 'name, z to a'){
                    order = 'name-dsc';}
                else if (selection == 'size, large to small'){
                    order = 'size';}
                else if (selection == 'size, small to large'){
                    order = 'size-asc';}           
            }

            var elements =$('.dropdown-menu').children();
            for ( var i = 0, l = elements.length; i < l; i++ ) {
                $(elements[i]).click(function(){            
                    SetOrder($(this).text());                    
                });
            }        
        }
        

        function RemoveElements () {
        // Delete unwanted buttons to restrict the possibility that users
        // bypass the inline stylesheet of this mako and gain access to 
        // certain history functionality on this page.
            $('.download-btn.icon-btn').remove();
            $('.icon-btn.show-selectors-btn').remove();
            $('.icon-btn.edit-btn').remove();
            $('.icon-btn.delete-btn').remove();
            $('.panel-menu.btn-group').remove();
            $('.panel-menu.btn-group').remove();
            $('.pull-left').remove();
            $('.create-new.btn.btn-default').remove();
            $('.done.btn.btn-default').remove();
            $('.history-drop-target-help').remove();
            $('.history-drop-target').remove();
            $('.panel-controls.clear.flex-row').remove();
            $('.errormessage').html("Permission denied: You are not allowed to view the ELN contents and the datasets associated with this history.")
            modal_checkbox_space = $('#include-deleted').parent().parent();
            $('#include-deleted').parent().remove();      
            FilterSubmittedEln(modal_checkbox_space);  
        }
        

        function FilterSubmittedEln (modal_checkbox_element) {
                    
            function CreateLabelBtn (id, text) {
                var new_form = document.createElement("label");
                new_form.innerHTML = "<input type='checkbox' id="+id+">"+text+"<br></input>"
                return new_form
            }
        
            new_form1 = CreateLabelBtn('both', 'Display all lab books');
            new_form2 = CreateLabelBtn('only_submitted', 'Display only submitted lab books');
            new_form3 = CreateLabelBtn('only_non_submitted', 'Display only non submitted lab books');
            modal_checkbox_space.append(new_form1).append(new_form2).append(new_form3);
                
            $("#"+sub_filter).prop("checked", true);
        
            // Setup listeners.
            $("#both").change(function() {
                sub_filter = 'both';
                UpdatePanelView();
            });
        
            $("#only_submitted").change(function() {
                sub_filter = 'only_submitted';
                UpdatePanelView();
            });
            
            $("#only_non_submitted").change(function() {
                sub_filter = 'only_non_submitted';
                UpdatePanelView();
            });
        }


        // TODO: The original page gets adjusted in a certain time interval,
        // because new elements are loaded under unpredictable conditions.       
        setInterval(function() {
            PageMask();
            RemoveElements();
        }, 500)


        </script>
            
        <!-- Modal for the project list -->
        <div id="modal_project_list" class="modal">
            <!-- Modal content -->
            <div class="modal-content" style="width: 480px;">            
                <div class="modal-header">
                    Select a project.
                    <span class="close" id="close_modal">X</span>
                </div>        
                <div class="modal-body" style ="margin-top:10px; margin-left:auto; margin-right:auto; width: 420px;">                
                    <select id="select_project_list" size="12" style=" width: 95%;" >
                    </select>                         
                </div>        
                <div class="modal-footer">  
                <button type="button" class="button" id="select_project_from_list_btn" >Select</button>   
                </div>
            </div>
        </div>
            
        <!-- Modal for the text search -->
        <div id="modal_text_search" class="modal">
            <!-- Modal content -->
            <div class="modal-content" style="width: 480px;">            
                <div id="text_search_modal_header" class="modal-header">
                    Please type in your search request.
                    <span class="close" id="close_text_search_modal">X</span>
                </div>        
                <div class="modal-body" style ="margin-top:10px; margin-left:auto; margin-right:auto; width: 420px;">                
                     <input type="text" id='text_search_input' style='width:100%;'>                   
                </div>        
                <div class="modal-footer">  
                <button type="button" class="button" id="search_txt_btn" >Search</button>   
                </div>
            </div>
        </div>
        
        <div class='page_eln_header'>
            <div class='page_header_logo_box'>
                <img src="${posixpath.join(images_url, 'pen.png')}" />
            </div>
            <div class='page_header_title_box'>
                Electronic Lab Notebook
            </div>
            <div class='page_header_action_box'>    
                <form action="${eln_config.get_url('show')}" method="get" id="go_back">
                    <input type="submit" value="Notebook" class="button">
                    <input type="hidden" name="dataset_id" id="dataset_id" value="${trans.security.encode_id(hda.id)}" class="button">
                </form>  
            </div>
        </div> 
        <div id="center"></div>
    </body>
</html>
</%def>
<!--   


                
-->
