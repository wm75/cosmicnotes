<%!
    import os
    import json
    import sys
    import posixpath
    import cgi
%>

<%def name="edit(eln_config)">
<%
    from eln import experiment
    from eln import db_modules


    # define urls and paths
    root = h.url_for("/")
    css_url = eln_config.get_url('stylesheets')
    images_url = eln_config.get_url('images') 
    current_history_contents_url = eln_config.get_url(
        'history_content'
        ).format(trans.security.encode_id(trans.history.id))
    datasets_display_url = eln_config.get_url('dataset_display')
    archive_path = eln_config.get_path('data_dir')

    current_date = experiment.timestamp()
    
    hist_name =  str(hda.history.name)
    # escape \ and ' in history names
    hist_name = hist_name.replace("\\","\\\\")      
    hist_name = hist_name.replace("'","\\'")        
    # pipe characters are reserved for use in file headers sent to cgi scripts
    hist_name = hist_name.replace("|","/")
    
    # Get the archive path for the page.
    current_labbook_hist_path = os.path.join(
        archive_path,
        experiment.get_page_path_from_id(hda.history.id)
        )
    
    project_title = 'not assigned to any project'
    if db_modules.get_pages_from_ids(eln_config.db_engine, [hda.history.id]):
        # Get project names, that are assigned to this history. 
        project_list = db_modules.get_projects_from_ids(
            eln_config.db_engine,
            db_modules.get_associated_project_ids(eln_config.db_engine, [hda.history.id])
            )
        project_title = ', '.join(cgi.escape(p['name']) for p in project_list)
    else:
        # This is a new page that we need to add to the database first
        db_modules.PageAccess(eln_config.db_engine).insert_page(
            id = hda.history.id,
            create_time = current_date,
            update_time = current_date,
            user_id = trans.user.id,
            user_username = trans.user.username,
            user_email = trans.user.email
            )
         
    # obtain a list of experiment sections sorted by section number and 
    # load each section's html content
    file_org = db_modules.get_sections(eln_config.db_engine, hda.history.id)
    for section in file_org:
        section.load_html_content(archive_path)

	# check if this is a submitted page, which should not be edited further
    lock_mode = False
    lock_file = os.path.join(current_labbook_hist_path, '.lock')
    if os.path.exists(lock_file):
        lock_mode = True 
            
    # Validate the current user.
    view_mode = False
    is_admin = False
    if trans.user.username != hda.history.user.username:
        view_mode = True
        lock_mode = True
    if trans.user_is_admin():    
        is_admin = True
        
    # Validate current hist with displayed hist.
    diverse_hist = False
    if hda.history.id != trans.history.id:
        diverse_hist = True             
%>

<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> ${visualization_name} </title>
    <!-- import CKEditor -->
    <script type="text/javascript" src="${eln_config.get_url('editor')}"></script>
    <!-- load css file  -->
    ${h.stylesheet_link(posixpath.join(css_url, 'eln.css'))}
    ${h.stylesheet_link(posixpath.join(css_url, 'CKeditor_content.css'))}
    <style>
        body {
        background-color: rgba(244,244,244,0.79);
        }      
    </style>
    <!-- use Galaxy's copy of jquery -->
    ${h.js('libs/jquery/jquery')}
  </head>
  <body>  
    <div class='page_container'>
        <div class='page_eln_header'>
            <div class='page_header_logo_box'>
                <img src="${posixpath.join(images_url, 'pen.png')}" />
            </div>
            <div class='page_header_title_box'>
                ${hist_name}
            </div>
            <div class='page_header_action_box'>    
              %if diverse_hist and not view_mode:
                <button class='warning_button' onclick ='GoToCurrentHist()' id='warning_button' title='The displayed history is not the current history. Click here to go to the current history note book.'>!</button>
              %endif
              %if diverse_hist and view_mode:
                <button class='button' onclick ='GoToCurrentHist()' id='other_hist_back_button' title='Go back to the current history note book.'>Go Back</button>
              %endif
                <button class='button' onclick ='GoToSettings()' id='settings_button'>Settings</button>   
                <button class='button' onclick ='GoToOverview()' id='overview_button'>Overview</button>
            </div>
        </div> 
        
        <div class='page_body'>
            <div id = "show_entries">
                <div style="margin-left:40px;">
                <b>Author:</b> ${hda.history.user.username} <br>
                <b>Projects:</b> ${project_title}
                </div>
            </div>
        </div>
        <div id='placeholder' style = 'margin:50px'></div>
        <div id = "footer" class="page_footer">
            <div class='page_header_action_box'>
                %if eln_config.export_enabled():
                    <button class="button" id="export_button" onclick="Export()" style="display:block;">Export</button> 
                %endif
                <button class="button" id="add_button" name="add_button" style="display:block;"></button> 
                <button class="button" id="submit_terminally_button" style="display:block;">Submit</button>
            </div>
        </div>   
    </div>
    
    <script> 
        var open_editor_no = 0,
            close_active_editor_upon_save = true,
            open_box,
            selected_box,
            history_name = '${hist_name}',
            username   = '${hda.history.user.username}',
            useremail = '${hda.history.user.email}',
            encoded_hist_id = '${trans.security.encode_id(hda.history.id)}'
            encoded_dataset_id = '${trans.security.encode_id(hda.id)}',
            last_part_no = 0,
            last_part_no_modified = 0,
            save_sync = true,
            show_baseurl = '${eln_config.get_url('show')}',
            history_contents_url = '${current_history_contents_url}',
            datasets_display_url = '${datasets_display_url}';
            
         
        // conversion of Python-level booleans to javascript ones
        lock_mode = ${int(lock_mode)} == 1;
        view_mode = ${int(view_mode)} == 1;
        is_admin = ${int(is_admin)} == 1;
        diverse_hist = ${int(diverse_hist)} == 1;
                
        function GoToSettings() {
            var leave_page = BeforeLeavePage()   
            if(leave_page){
                window.location.href = show_baseurl + '?dataset_id=' +
                    encoded_dataset_id + '&settings=' + encoded_hist_id;
            }
        }       

        function GoToShowHistory(section_no) {
            var leave_page = BeforeLeavePage() 
            if(leave_page){
                window.location.href = show_baseurl + 
                    '?dataset_id=' + encoded_dataset_id +
                    '&show_history=' + encoded_hist_id +
                    '&section_no=' + section_no +
                    '&version_a=&version_b=';
            }            
        }        
        
        function GoToOverview() {
            var leave_page = BeforeLeavePage() 
            if(leave_page){
                window.location.href = show_baseurl +
                    '?dataset_id=' + encoded_dataset_id + '&overview=true';
            }            
        }   
        
        function Export() {
            url = '${current_history_contents_url}';
            $.ajax( {
                url:    url,
                async:  false,
                type:   'GET',
                datatype: 'json',
                success: function (data) {
                    url = show_baseurl;
                    response = {
                        dataset_id: encoded_dataset_id,
                        export: encoded_hist_id,
                        history_contents: JSON.stringify(data)
                    }
                    $.ajax( {
                        url:    url,
                        async:  false,
                        type:   'POST',
                        data:   response
                    });
                }  
            });          
        }   
        
        function GoToCurrentHist() {
            var leave_page = BeforeLeavePage() 
            if(leave_page){
                
                url = '${current_history_contents_url}';
                $.ajax( {
                    url:    url,
                    async:  false,
                    type:   'GET',
                    success: function (data) {
                        first_dataset = data[0];
                        dataset_id = first_dataset['id'];
                        window.location.href = show_baseurl +
                            '?dataset_id=' + dataset_id;
                        
                        }
                });
            }
        }
                
        function SwitchEditor(part_number, scroll_down) {            
            // set up default arguments
            scroll_down = typeof scroll_down !== 'undefined' ? scroll_down : false;
            
            if (open_editor_no > 0) {
                // handle currently opened editor
                Autosave();
				CloseEditor(open_editor_no);
				open_editor_no = 0;
            }

            var ckeditor = CKEDITOR.replace('box_' + part_number);
	        // waits for ckeditor to load completely and scroll to bottom of the page
	        if (scroll_down) {
	            ckeditor.on("instanceReady", function() {
	                // JQuery gives us cross-browser compatibility here.
	                // window.scrollTo(0, $(document).height());
	                document.getElementById('entry_box_' + part_number).scrollIntoView(false);
	            });
	        }

			document.getElementById("add_button").style.display = 'none';
			document.getElementById("submit_terminally_button").style.display = 'none';
            document.getElementById("button_edit_" + part_number).style.display = 'none';
            document.getElementById("save_button_" + part_number).style.display = 'block';
            document.getElementById("button_close_" + part_number).style.display = 'block';                            
            open_editor_no = part_number;      
            open_editor_old_content = CKEDITOR.instances['box_' + part_number].getData();
            
            // (Re)start the autosave timer
            if (typeof autosave_timer !== 'undefined'){
                window.clearInterval(autosave_timer);    
            }
            autosave_timer = window.setInterval(function(){ Autosave(); }, ${eln_config.get_autosave_interval()});
        } 
        
        function CloseAndDiscard(section_no) {
            // called when a section's Close & Discard button is clicked
            // only does anything if the section number is that of the 
            // currently opened editor.
            if (section_no == open_editor_no) {
			    // Restores old saved entry
		        selected_box = "box_" + section_no;
		        CKEDITOR.instances[selected_box].setData(
		            open_editor_old_content, function (){
		        		CloseEditor(section_no);
		        	}
		        );
		        open_editor_no = 0;
		    }
        }
            
		function CloseEditor(sec_no) {
            open_box = "box_" + sec_no;
            var this_editor_content = CKEDITOR.instances[open_box].getData();
            
            CKEDITOR.instances[open_box].destroy();

			// if (!$.contains("entry_box_" + sec_no, 'erratum_head_' + sec_no)) {
			if (!lock_mode) {
				document.getElementById("button_edit_" + sec_no).style.display = 'block';
				document.getElementById("submit_terminally_button").style.display = 'block';
			}
			if (!view_mode) {
				document.getElementById("add_button").style.display = 'block';
            }
            document.getElementById("save_button_" + sec_no).style.display = 'none';
            document.getElementById("button_close_" + sec_no).style.display = 'none'; 
            
            if (sec_no == last_part_no_modified) {
                if (this_editor_content === ""){
                    if (typeof section_no != 'undefined'){
                        var added_box = document.getElementById("entry_box_" + sec_no);     
                        added_box.parentNode.removeChild(added_box); 
                        // when box is removed set last_part_no_modified back to last_part_no  
                        last_part_no_modified = last_part_no;                           
                    }
                }
                else {
                    // when new box is saved set last_part_no to last_part_no_modified
                    last_part_no = last_part_no_modified;                           
                }    
            }		
		}
		
        function Save(part_number) {
            if (lock_mode)  {
                if (!confirm("Are you sure you want to save this erratum?\nYou won't be able to edit it afterwards.")) {
                return false;
                }
            }
            selected_box = "box_" + part_number;
            editor_user_input = CKEDITOR.instances[selected_box].getData();
            // Checks if the editor is empty and breaks the submit.     
            if (editor_user_input === ""){
                //alert("You have to type something in the editor to save its content!");
                return false;
            }
            if (editor_user_input.trim() != open_editor_old_content.trim()) {
                var project_part = part_number;
                url = show_baseurl;     
                request = {request_type: 'save_section',
                           params: {page_id: encoded_hist_id,
                                    section_no: project_part,
                                    is_erratum: lock_mode}
                                    }
                $.ajax( {
                    url: url,
                    async: save_sync,
                    type:  'POST',
                    data:  {
                            eln_request: request.request_type,
                            eln_request_params: JSON.stringify(request.params),
                            eln_request_data: editor_user_input
                           },
                    success: function (response) {
						// Update timestamps according to the section information returned
						// by the save request.       
                        response = JSON.parse(response);
                		var creation_date = response['create_time'].substring(0, response['create_time'].length - 9);
                		var last_modify_date = response['update_time'].substring(0, response['update_time'].length - 9);
						var create_date_div = document.getElementById("show_date_" + part_number);
						create_date_div.innerHTML = "created on: " + creation_date;
						if (creation_date != last_modify_date) {
						    var last_modify_div = document.getElementById("show_last_modify_" + part_number);
						    last_modify_div.innerHTML = "last modified on: " + last_modify_date;
						    last_modify_div.style.display = 'inline';
						    document.getElementById("repo_hist_button_" + part_number).style.display = 'inline';
						}             
                    }
                });  
            }
            
            // When checkbox is active close editor, otherwise save the new content in var.
            if (close_active_editor_upon_save) {     
                CloseEditor(open_editor_no);
                open_editor_no = 0;
            }
            else {
                open_editor_old_content = CKEDITOR.instances['box_'+ part_number].getData();
            }
            
            // When lock-mode is active, hide edit button
            if (lock_mode)  {
                document.getElementById("button_edit_"+ part_number).style.display = 'none';
            }
            else {
		        // With at least one section saved, it is safe to show the add and submit buttons
		        document.getElementById("add_button").style.display = 'block';
		        document.getElementById("submit_terminally_button").style.display = 'block';
		    }
        }
        
        function BeforeLeavePage () {
            // checks for an opened editor
            if (open_editor_no > 0) {
            
                open_box = "box_"+open_editor_no;
                editor_user_input = CKEDITOR.instances[open_box].getData();
                
                if ((editor_user_input.trim() == open_editor_old_content.trim())||(editor_user_input === "")) {
                    return true;
                }
                if (confirm("You're about to leave the your lab book with an opened editor.\n Do you want to save its content?") == true) {   
                    save_sync = false;             
                    Autosave();
                    CloseEditor(open_editor_no);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return true;
            }
        }
    
        function SubmitTerminally () {
            // checks for an opened editor
            if (open_editor_no > 0) {
                if (confirm("You're about to submit your lab book with a section opened for editing.\nDo you want to save its content?")) {
                    Autosave();
                    CloseEditor(open_editor_no);
                }
                else {
                    return false;
                }  
                return false;
            }
            else {
                if (confirm("Are you sure you want to submit this project?\nYou won't be able to edit your notebook afterwards.")) { 
                    var url = show_baseurl;
                    var request = {request_type: 'submit',
                                   params: {page_id: encoded_hist_id}
                                  }
                    $.ajax( {
                        url: url,
                        async: false,
                        type:  'POST',
                        data:  {
                                eln_request: request.request_type,
                                eln_request_params: JSON.stringify(request.params),
                                eln_request_data: ''
                               } 
                    });
                    
                    window.location.href = show_baseurl +
                                           '?dataset_id=' + encoded_dataset_id;
                }
                else {  
                    return false;        
                } 
            }                    
        }
         
        function AddEntry (date_created, date_updated, is_erratum, section_content) {
        // Onclick add-button this function creats a new empty entry form and new buttons.
        // The entry number is given trough the argument. The creation_date is set to the
        // current date. After this function is called.
        	// set up default arguments
        	date_created = typeof date_created !== 'undefined' ? date_created : null;
        	date_updated = typeof date_updated !== 'undefined' ? date_updated : null;
            is_erratum = typeof is_erratum !== 'undefined' ? is_erratum : false;
            section_content = typeof section_content !== 'undefined' ? section_content : '';  
        	
            section_no = last_part_no_modified + 1;
            last_part_no_modified = section_no;
                    
            var save_button = document.createElement("button");
            save_button.setAttribute('id', "save_button_" + section_no); 
            save_button.setAttribute('class', "button");
            save_button.setAttribute('style', 'display: none');
            save_button.setAttribute('onclick',
                                     'Save(' + section_no + ')'); 
            save_button.innerHTML= "Save";

            var create_date = document.createElement("div");
            create_date.setAttribute('id',"show_date_" + section_no);
            create_date.setAttribute('class',"date_header_text");
            create_date.setAttribute('style', 'float: left');
            if (date_created) {
                create_date.innerHTML = "created on: " + date_created;
            }
            else {
                create_date.innerHTML = "";
            }
            
			var diff_btn = document.createElement("div");
            diff_btn.setAttribute('id',"repo_hist_button_" + section_no);
            diff_btn.setAttribute('class',"button inline-button");
            diff_btn.setAttribute('onclick',
                                  'GoToShowHistory(' + section_no + ')');
            if (date_updated) {
                diff_btn.setAttribute('style', 'display: inline');
            }
            else {
            	diff_btn.setAttribute('style', 'display: none');
            }
            diff_btn.innerHTML = "<->";

            var update_date = document.createElement("div");
            update_date.setAttribute('id',"show_last_modify_" + section_no);
            update_date.setAttribute('class',"last_modify_header_text");
            if (date_updated) {
                update_date.innerHTML = "last modified on: " + date_updated;
            }
            else {
				update_date.setAttribute('style', 'display: none');
				update_date.innerHTML = "";
			}
            
            var editor_container = document.createElement("div");
            editor_container.setAttribute('class', "editor_container");
            
            var txt_field = document.createElement("div");
            txt_field.setAttribute('class', "show_window");
            txt_field.setAttribute('id', "box_" + section_no);
            txt_field.innerHTML = section_content;
              
            var close_button = document.createElement("button");
            close_button.setAttribute('type', "button");
            close_button.setAttribute('class', "button");
            close_button.setAttribute('id', "button_close_" + section_no);
            close_button.setAttribute('style', 'display: none');
            close_button.setAttribute('onclick', 
            						  'CloseAndDiscard(' + section_no + ')');
            close_button.innerHTML = "Close & Discard";
    
            var edit_button = document.createElement("button");
            edit_button.setAttribute('type', "button");
            edit_button.setAttribute('id', "button_edit_" + section_no);
            edit_button.setAttribute('class', "button");
            edit_button.setAttribute('onclick',
                                     'SwitchEditor(' + section_no + ')');
            edit_button.innerHTML = 'Edit';
            
            var show_erratum = document.createElement("div");
            show_erratum.setAttribute('id', "erratum_head_" + section_no);
            show_erratum.setAttribute('class', "erratum_header_text");
            show_erratum.setAttribute('style', 'display: inline-block'); 
            show_erratum.innerHTML = 'Erratum';
            
            var new_entry_box = document.createElement("div");
            new_entry_box.setAttribute('id', "entry_box_" + section_no);
            
            var new_top_section = document.createElement("div");
            new_top_section.setAttribute('id', "top_entry_section");
            new_top_section.setAttribute('class', "entry_header_box");
            
            new_top_section.appendChild(create_date);
            new_top_section.appendChild(diff_btn);
            new_top_section.appendChild(update_date);
            if (is_erratum) {
            	new_top_section.appendChild(show_erratum);
            }
            new_top_section.appendChild(close_button);
            new_top_section.appendChild(edit_button);
            new_top_section.appendChild(save_button);
            editor_container.appendChild(new_top_section);
            editor_container.appendChild(txt_field);
            new_entry_box.appendChild(editor_container);
            document.getElementById("show_entries").appendChild(new_entry_box);  
        }        
            
        function Autosave() {
            if (open_editor_no > 0) {
                close_active_editor_upon_save = false;
                var SaveButton = document.getElementById("save_button_" + open_editor_no);
                SaveButton.click();
                close_active_editor_upon_save = true;
            }
        }         
        
        // Show History, visibility 
        %for entry in file_org:
        	AddEntry(
        		"${entry.create_time.date()}",
        	  %if entry.is_modified():
        		"${entry.update_time.date()}",
        	  %else:
        	    null,
        	  %endif
        	  %if entry.is_erratum:
        	    true,
        	  %else:
        	    false,
        	  %endif
                ${json.dumps(entry.html_content or '')}
        	)
        	last_part_no += 1;
        %endfor
        
        if (!lock_mode) {
            // adjust text and action of add button
            var add_btn = document.getElementById("add_button");
		    add_btn.onclick = function () {
		        AddEntry();
		        SwitchEditor(last_part_no_modified, scroll_down=true);
		    };
		    add_btn.innerHTML = 'Add Section';
		    // activate submit button
		    document.getElementById("submit_terminally_button").onclick = SubmitTerminally;
          %if not file_org or file_org[-1].create_time.date() != current_date.date():
	        AddEntry();
	        document.getElementById("button_close_" + last_part_no_modified).style.display = 'none';	        
	      %endif
            SwitchEditor(last_part_no_modified, scroll_down=true);
        }      
        else {
            // lock-mode active
			// adjust text and action of add button
			var add_btn = document.getElementById("add_button");
			add_btn.onclick = function () {
			    AddEntry(null, null, true);
			    SwitchEditor(last_part_no_modified, scroll_down=true);
			};
			add_btn.innerHTML = 'Add Erratum';
			// hide submit and section edit buttons
			document.getElementById("submit_terminally_button").style.display = 'none';
            %for section_no, entry in enumerate(file_org, 1):
               document.getElementById("button_edit_"+${section_no}).style.display = 'none';   
            %endfor
        }
    
        // View mode doesn't show add and submit buttons and 
        // shows settings button only for Administrators
        if (view_mode){
            document.getElementById("add_button").style.display = 'none';
            document.getElementById("submit_terminally_button").style.display = 'none';
            if (!is_admin){
                document.getElementById("settings_button").style.display = 'none';
            }
        }
        
        // execute Autosave before leaving the page
        window.addEventListener("beforeunload", function(e){
            save_sync = false;
            Autosave();
        }, false);     
                           
    </script>
  </body>
</html>
</%def>
