<%def name="show_history(eln_config, encoded_hist_id, section_no, version_a_hash='', version_b_hash='')">
<%
import os
import posixpath
import copy
import difflib
import json

from eln import eln_util
from eln import db_modules
from eln import experiment
from eln import githandling


# define urls and paths
css_url = eln_config.get_url('stylesheets')
images_url = eln_config.get_url('images')


page_id = trans.security.decode_id(encoded_hist_id)
# Using page_id and section_no retrieve the Section from the database.
section = db_modules.get_section(
    eln_config.db_engine, page_id, section_no
    )
section_copy = copy.copy(section)

archive_path = eln_config.get_path('data_dir')
page_path = os.path.join(
    archive_path, experiment.get_page_path_from_id(page_id)
    )
file_path = os.path.join(page_path, section.fname)
temp_path = os.path.join(page_path, 'temp')
temp_file_path = os.path.join(temp_path, file_path)

# Reads the commit message of the file and extracts all commit dates and hashes.
git_session_tip = githandling.GitCall(page_path)
commit_history = git_session_tip.extract_commit_history(
    section.fname, report_pending=True
    )
commit_dates = [commit_info[0][:10] for commit_info in commit_history]
hashes = [commit_info[1] for commit_info in commit_history]

# Determine the commits that we need.
if not version_a_hash:
    version_a_index = len(commit_history) - 1
elif version_a_hash == '--':
    version_a_index = 0
else:
    version_a_index = hashes.index(version_a_hash)
if not version_b_hash or version_b_hash == 'current':
    version_b_index = 0
else:
    version_b_index = hashes.index(version_b_hash)


# try to acquire the section contents of the two versions in the cheapest
# possible way:
# - if any of the two versions is the current version, its content can be
#   loaded from the archive repo directly without cloning
# - older versions require the repo to be cloned and the section file to be
#   checked out in the right version
# - if version b is the same as version a, however, version b content does not
#   have to be loaded at all, but can simply be copied from version a
if version_a_index == 0:
    section.load_rst_content(archive_path)
else:
    with git_session_tip.tmp_clone(archive_path) as tmp_path:
        git_session_tmp = githandling.GitCall(tmp_path)
        git_session_tmp.checkout(
            hashes[version_a_index],
            section.fname
        ).communicate()
        section.load_rst_content(os.path.dirname(tmp_path))
if version_b_index == version_a_index:
    section_copy.content = section.content
    section_copy.html_content = section.html_content
elif version_b_index == 0:
    section_copy.load_rst_content(archive_path)
else:
    with git_session_tip.tmp_clone(archive_path) as tmp_path:
        git_session_tmp = githandling.GitCall(tmp_path)
        git_session_tmp.checkout(
            hashes[version_b_index],
            section_copy.fname
        ).communicate()
        section_copy.load_rst_content(os.path.dirname(tmp_path))

# generate complete html diff table
difflib_obj = difflib.HtmlDiff()
diff_table = difflib_obj.make_file(
    section.content.split('\n'),
    section_copy.content.split("\n")
    )
# new font for the table
diff_table = diff_table.replace("Courier", "Roboto", 1)
    
# Create an insertable option tag for the js dropdown menus containing the commit dates.
commit_select_options = eln_util.build_select_options(
    (date, i) for i, date in enumerate(commit_dates)
    )
%>
    

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- load css file  -->
        ${h.stylesheet_link(posixpath.join(css_url, 'eln.css'))}
        ${h.stylesheet_link(posixpath.join(css_url, 'CKeditor_content.css'))}       
        <script type="text/javascript">
            var html_versions = ${json.dumps([
                                      section.html_content,
                                      section_copy.html_content
                                      ])};
            var hashes = ${json.dumps(hashes)}
                        
            function Update() {         
                // Reloads this page, with the currently selected parameters. 
            
                var from_date_form = document.getElementById("dropdown_from");
                var to_date_form = document.getElementById("dropdown_to");
                
                window.location.href = '${
                    eln_config.get_url('show')
                    }?dataset_id=${trans.security.encode_id(hda.id)}' +
                    '&show_history=${encoded_hist_id}' +
                    '&section_no=${section_no}' +
                    '&version_a=' + hashes[from_date_form.selectedIndex] +
                    '&version_b=' + hashes[to_date_form.selectedIndex];
            }   
        </script>    
    </head>
    <body>
    <div class='page_container'>       
        <div class='page_eln_header' style='min-height:none;'>
            <div class='page_header_logo_box'>
                 <img src="${posixpath.join(images_url, 'pen.png')}"/>
            </div>
            <div class='page_header_title_box'>
                Show History
            </div>
            <div class='page_header_action_box'>    
                <form action="${eln_config.get_url('show')}" method="get" id="go_back">
                    <input type="submit" value="Back" class="button">
                    <input type="hidden" name="dataset_id" id="dataset_id" value='${trans.security.encode_id(hda.id)}' class="button">
                </form>  
            </div>
        </div> 
        
        <div class='page_body'>
            <div id="diff_table_head">      
                <div id='diff_table_head_left'>
                    <div class='diff_table_head_el' style='float:left;margin-left:20px;'>
                        <button class='eye_button' title='Show Version' id="show_from" onclick="show_modal(0)">
                            <img src="${posixpath.join(images_url, 'eye.png')}"/>
                        </button>
                    </div>
                    <div class= "diff_table_head_el" style='float:right;margin-right:20px;'>
                        ${commit_dates[version_a_index]}
                    </div> 
                </div>
                <div id='diff_table_head_center'>
                    <div id ="left_dropdown_box">
                        <div class= "diff_table_head_el" style='text-align:center;'>
                            <form>
                                <select id="dropdown_from">
                                    ${'\n'.join(commit_select_options)}
                                </select>
                            </form>
                        </div>
                    </div>
                    <div id = "pos_update_button" class= "diff_table_head_el">
                        <div class= "diff_table_head_el" style='text-align:center;'>
                            <button class='button' style='float:none;' onclick='Update()'>Update</button>
                        </div>
                    </div>        
                    <div id = "right_dropdown_box" class= "diff_table_head_el">
                        <div class= "diff_table_head_el" style='text-align:center;'>
                            <form>
                                <select id="dropdown_to">
                                    ${'\n'.join(commit_select_options)}
                                </select>
                            </form>    
                        </div>    
                    </div>
                </div>       
                <div id='diff_table_head_right'>
                    <div class= "diff_table_head_el" style='float:left;margin-left:20px;'>
                        ${commit_dates[version_b_index]}
                    </div>
                    <div class= "diff_table_head_el" style='float:right;margin-right:20px;'>
                        <button class='eye_button' title='Show Version' id="show_from" onclick="show_modal(1)">
                            <img src="${posixpath.join(images_url, 'eye.png')}"/>
                        </button>
                    </div>
                </div>       
            </div>
               
            <div class ="diff_table_window">
                <div id="dic_table" style="float:none">
                    ${diff_table}
                </div>
            </div>
            <!-- Modal -->
            <div id="modal" class="modal">
                <!-- Modal content -->
                <div class="modal-content-show_hist">            
                    <div class="modal-header">
                        <span class="close" id="close_modal">x</span>
                    </div>        
                    <div class="modal-body">                
                        <div style ="margin-left: auto; margin-right: auto;width: 80%px ;">
                            <div id="show_content_modal">
                            </div>
                        </div>         
                    </div>        
                    <div class="modal-footer">     
                    </div>
                </div>
            </div>      
        </div>    
    </div>
    
    <script>
        // Get the modal
        var modal = document.getElementById('modal');
        // Get the <span> element that closes the modal
        var close_button_modal = document.getElementById("close_modal");

        // When the user clicks on the button, open the modal
        function show_modal (html_content_i) {             
            dialog = document.getElementById("show_content_modal");
            dialog.innerHTML = html_versions[html_content_i];
            modal.style.display = "block";
            }
        // When the user clicks on <span> (x), close the modal
        close_button_modal.onclick = function() { 
            modal.style.display = "none";
            }
            
        document.getElementById("dropdown_from").value = "${version_a_index}";
        document.getElementById("dropdown_to").value = "${version_b_index}";
    </script>
        
    </body>
</html>
</%def>

