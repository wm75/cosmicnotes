<%def name="overview_response(trans, eln_config)">
<%
import json

from eln import db_modules


# define urls and paths

galaxy_histories = json.loads(trans.request.params['overview_histdict'])
query_author = trans.request.params.get('author', '')
order = trans.request.params.get('order') or 'update_time'
sub_filter = trans.request.params.get('sub_filter') or 'both'
text_search = trans.request.params.get('text_search', '')
if trans.request.params.get('project'):
    query_project = trans.security.decode_id(trans.request.params['project'])
else:
    query_project = None


for hist in galaxy_histories:
    hist['id'] = trans.security.decode_id(hist['id'])
    hist['user_id'] = trans.security.decode_id(hist['user_id'])

galaxy_history_ids = {hist['id'] for hist in galaxy_histories}
hist_assoc_projects = db_modules.get_projects_from_ids(
    eln_config.db_engine,
    db_modules.get_associated_project_ids(
        eln_config.db_engine, galaxy_history_ids
        )
    )
hist_assoc_users = db_modules.get_users_from_ids(
    eln_config.db_engine,
    {hist['user_id'] for hist in galaxy_histories}
    )
history_mapping = {h['id']: h['name'] for h in galaxy_histories}
user_mapping = {u['id']: u['username'] for u in hist_assoc_users}


# Generates a history dictionary dependent of the user-search and filter.
# The dictionary contains all necessary information for the multi-panel view. 
pages, eln_hits = db_modules.collect_filtered_pages_metadata(
    eln_config.db_engine,
    query_author,
    query_project,
    text_search,
    sub_filter
    )

# Form the intersection between the histories Galaxy knows about and the
# filtered ELN pages.
pages = [page for page in pages if page['id'] in galaxy_history_ids]

# Modify each page metadata to be compatible with the format and to include
# all the information that the calling javascript expects:
# copy the page name from the corresponding Galaxy history name,
# replace the user_id field with a username field,
# add a list of associated project names,
# convert datetime onjects to strings,
# indicate whether the page was in the full-text search hit list,
# encode the page/history id

for page in pages:
    page['name'] = history_mapping[page['id']]
    page_project_ids = db_modules.get_associated_project_ids(
        eln_config.db_engine, [page['id']]
        )
    page['project_list'] = [
        p['name'] for p in hist_assoc_projects if p['id'] in page_project_ids
        ]
    page['username'] = user_mapping[page.pop('user_id')]
    page['create_time'] = str(page['create_time'])
    page['update_time'] = str(page['update_time'])
    page['commit_time'] = str(page.get('commit_time', ''))
    page['fulltext_search_hit'] = page['id'] in eln_hits
    page['id'] = trans.security.encode_id(page['id'])

%>

${json.dumps({
      'pages': pages,
      'authors': [u['username'] for u in hist_assoc_users],
      'projects': {p['name']: trans.security.encode_id(p['id'])
                   for p in hist_assoc_projects}
      })}

</%def>
