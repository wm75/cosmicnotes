strformat = function(template) {
    // http://stackoverflow.com/questions/610406
    var args = Array.prototype.slice.call(arguments, 1);
    return template.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number] 
        : match
      ;
    });
  };
  

CKEDITOR.dialog.add( 'datasetlinkDialog', function( editor ) {
    return {
        title: 'Dataset Link',
        minWidth: 400,
        minHeight: 200,
        contents: [
        {
            id: 'tab1',                 //window tab
            label: 'Current History',
            elements: [
                
                {
                type : 'text',
                id : 'contents',
                label : 'Text to display for the link', 
                setup: function( selection ) {
                     this.setValue( selection );
                    },          
                },
                {
                type: 'select',           //elements
                id: 'checkbox',
                label: 'Dataset to link to:',
                items: [  ],               // objects in the checkbox
                onShow: function() {  
                    this.clear();                    
                    var i;                  
                    for (i=0; i<history_contents.length; i++) { 
                        this.add(history_contents[i]['hid'], [i]);            
                        }
                    }
                }
            ]
            
        },
/* tab2 commented out in current version
        {
            id: 'tab2',                 //window tab
            label: 'Other Histories',
            elements: [
                {
                type : 'text',
                id : 'contents',
                label : 'Displayed Text',
                setup: function( selection ) {
                this.setValue( selection );
                    }    
                },
         
                {
                type: 'select',           //elements
                id: 'checkbox_hist',
                label: 'Please select your history:',
                items: [  ],               // objects in the checkbox        
                onShow: function() { 
                    history_select = this;
                    history_select.clear();
                    $.each(other_hist_data, function(i, val) {
                        if (!$.isEmptyObject(val[0])) {
                            history_select.add(val[1], [i]);   
                        }       
                    })
                },
                onChange: function () {
                    anno_select.clear();
                    hist_selection = history_select.getValue();
                    anno_obj = other_hist_data[hist_selection][0]
                    $.each(anno_obj, function(anno_id, val) {
                        anno_select.add(val[0], [anno_id]);   
                    })
                }                                  
                },
                                        
                {
                type: 'select',           //elements
                id: 'checkbox_anno',
                label: 'Please select your annotation:',
                items: [  ],               // objects in the checkbox        
                onShow: function() { 
                    anno_select = this;
                    anno_select.clear();
                    hist_selection = history_select.getValue();
                    anno_obj = other_hist_data[hist_selection][0]
                    $.each(anno_obj, function(anno_id, val) {
                        anno_select.add(val[0], [anno_id]);   
                    })

                    }                                  
                     
                }
            ]
        }
        end of outcommented tab2 */           
        ],

        onShow: function() {
            var selection = editor.getSelection().getSelectedText();
            this.setupContent( selection );
            
            $.ajax( {
                url:    history_contents_url,
                async:  false,
                type:   'GET',
                datatype: 'json'
                }
            ).done(function(contents) {
                history_contents = contents;
            })
            
            /* tab2 functionality disabled in current version 
            if (typeof other_hist_data === 'undefined' || !other_hist_data ) {
                url = root + 'visualization/show/cosmicnotes?dataset_id=' +
                      encoded_dataset_id;
                address =  window.location.protocol + "//" + 
                           window.location.host + root;
                $.ajax( {
                    url:    url,
                    async:  false,
                    type:   'GET',
                    dataType: "json",
                    data: {annolink: historyId,
                           root: address,
                           user: username,
                        }, 
                    success: function (data) {
                        other_hist_data = data;
                        }
                });
            } */
      
            // hide tab1 when there is no annotation in the displayed hist. 
            if ($.isEmptyObject(history_contents)) {
                this.hidePage( 'tab1' );
            }
            
            // hide tab1 when the current hist is not the displayed hist  
            if ( diverse_hist ) {
                this.hidePage( 'tab1' );      //Hide tab.
            }
            
            /* disbaled tab2 in current version
            if ($.isEmptyObject(other_hist_data)) {
                this.hidePage( 'tab2' );      //Hide tab.
            }
            */
            
            if ((diverse_hist || $.isEmptyObject(history_contents))
                // disabled tab2 behaviour
                // && $.isEmptyObject(other_hist_data 
                ){
                this.hidePage( 'tab1' );
                // this.hidePage( 'tab2' ); 
            }
            else {
                this.hidePage( 'tab3' );
            }        
        },            

        onCancel: function () {
            this.showPage( 'tab1' );  
            // this.showPage( 'tab2' );  
            this.showPage( 'tab3' );
            // dummy operation: write empty string into document
            editor.insertHtml( '' );
        },

        onOk: function() {          // user clicks OK
            var CurrObj = CKEDITOR.dialog.getCurrent();
            var current_tab = CurrObj.definition.dialog._.currentTabId;
            this.showPage( 'tab1' );
            // this.showPage( 'tab2' );
            this.showPage( 'tab3' );

            if(current_tab == 'tab1') {
                var textarea_content = this.getContentElement( 'tab1', 'contents' ).getValue();
                var checkbox_value = this.getContentElement( 'tab1', 'checkbox' ).getValue();
                link_url = strformat(
                    datasets_display_url,
                    history_contents[checkbox_value]['id']
                    );
                if (textarea_content == ""){
                    // no link text provided by user
                    // use Dataset# as default
                    editor.insertHtml( '<a href=' + link_url + '>Dataset ' + history_contents[checkbox_value]['hid'] + '</a>' );
                }
                else {
                    // insert a link with user-provided text
                    editor.insertHtml( '<a href=' + link_url + '>' + textarea_content + '</a>' );
                }
            } 
/* disable tab2 in current version            
            else if (current_tab == 'tab2') {
                var text_content = this.getContentElement( 'tab2', 'contents' ).getValue();
                var out_hist_id = this.getContentElement( 'tab2', 'checkbox_hist' ).getValue();
                var out_anno_id = this.getContentElement( 'tab2', 'checkbox_anno' ).getValue();
                var url = other_hist_data[out_hist_id][0][out_anno_id][1]
                
                if (text_content != ''){
                    
                    var link_name = text_content
                    editor.insertHtml( '<a href='+url+'>'+link_name+'</a>' );
                    
                }
                else {
                    
                    var link_name = other_hist_data[out_hist_id][0][out_anno_id][0]
                    editor.insertHtml( '<a href='+url+'>'+link_name+'</a>' );
                }
            
            }
            end of disabled tab2 block */
            else {
                // dummy operation: write empty string into document
                editor.insertHtml( '' );
            }
        }
    };
});
//    }

      

