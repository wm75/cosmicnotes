CKEDITOR.plugins.add( 'datasetlink', {	//add a new plugin to ckeditor
    icons: 'datasetlink',			//icon have to be in he correct dir
    init: function( editor ) {		//initilaze plugin
        editor.addCommand( 'datasetlink', new CKEDITOR.dialogCommand( 'datasetlinkDialog' ) ); // adds new Command to the plugin, in this case a new Dialog
        editor.ui.addButton( 'Datasetlink', { // add Button to the editor
            label: 'Dataset Link',
            command: 'datasetlink',
            toolbar: 'links,10'		//sets the toolbar group and the position in the toolbar
        });

        CKEDITOR.dialog.add( 'datasetlinkDialog', this.path + 'dialogs/datasetlink.js' ); //sets up the dialog path 
    }
});
