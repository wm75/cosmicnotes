import os
import datetime
import codecs

import pypandoc

import githandling
import db_modules


def save_section (eln_config, section, editor_txt, timestamp):
    # set paths and read database
    page = db_modules.get_pages_from_ids(
        eln_config.db_engine, [section.page_id]
        )[0]
    page_sections = db_modules.get_sections(
        eln_config.db_engine, section.page_id
        )
    archive_path = eln_config.get_path('data_dir')
    page_path = os.path.join(archive_path, page.fpath)
    section_path = os.path.join(page_path, section.fname)

    gitcall = githandling.GitCall(page_path)

    if page.commit_time is None or page.update_time > page.commit_time:
        # The current page contains previous uncomitted changes.
        # If any of these are from a previous day, i.e., the update_time
        # of the corresponding section is older than beginning of today,
        # we want to commit them before saving the current content.

        # define "beginning of today"
        due_datetime = datetime.datetime(
            timestamp.year,
            timestamp.month,
            timestamp.day
            )
        # check each section and stage the corresponding file if appropriate
        staged_sections = []
        for page_section in page_sections:
            if (page_section.commit_time is None or page_section.update_time
                > page_section.commit_time) and (due_datetime
                                                 > page_section.update_time):
                    gitcall.add(page_section.fname).communicate()
                    staged_sections.append(page_section)

        # Commit all pending changes!
        commit_message = githandling.get_commit_message(
            gitcall, heading='Regular commit of page activities.'
            )
        gitcall.commit(
            commit_message, page.update_time,
            page.user_username, page.user_email
            ).communicate()
        
        # Update the database to reflect the new commit_time
        db_section_access = db_modules.SectionAccess(eln_config.db_engine)
        for staged_section in staged_sections:
            staged_section.commit_time = page.update_time
            db_section_access.update_section(staged_section)


    # Check if saving the current file is actually necessary.
    # There are two cases when saving a regular section is not necessary:
    #  i) the section has been saved before and saving would not result
    #     in a change to the existing file,
    # ii) the section has not been saved before and the content to save
    #     consists of whitespace only.
    # Errata, however, always need saving.
    need_save = True
    if not section.is_erratum:
        # format conversion to plain text
        editor_txt_plain = pypandoc.convert(
            editor_txt, 'plain', format='html'
        )
        if os.path.isfile(section_path):
            with codecs.open(section_path, 'r', encoding='utf8') as source:
                header = source.readline().strip('\n')
                current_file_content = source.read()
                if current_file_content == section.content:
                    need_save = False
        elif editor_txt_plain.isspace():
            need_save = False
        
    if need_save:
        with codecs.open(section_path, 'w', encoding='utf8') as file_html:
            file_html.write('<!-- cosmicnotes page section -->\n'
                            + editor_txt)
        if section.is_erratum:
            # git add/commit errata immediately
            gitcall.add(section_path).communicate()
            commit_message = githandling.get_commit_message(
                gitcall, heading='An erratum has been added to the page.'
            )
            gitcall.commit(
                commit_message, timestamp,
                page.user_username, page.user_email
                ).communicate()

        # update the database
        db_section_access = db_modules.SectionAccess(eln_config.db_engine)
        section.update_time = timestamp
        if section.is_erratum:
            section.commit_time = timestamp
        # Disable fulltext storage for now.
        # if eln_config.db_is_postgres() or eln_config.db_is_mysql():
        #     editor_txt_plain = editor_txt_plain[:21000]
        #     section.content = editor_txt_plain
        if section.id is None:
            # a new section is to be added
            section.create_time = timestamp
            db_section_access.insert_section(section)
        else:
            db_section_access.update_section(section)
    return section


def submit_page (eln_config, page_id, timestamp):
    # set paths and read database
    page = db_modules.get_pages_from_ids(
        eln_config.db_engine, [page_id]
        )[0]
    page_sections = db_modules.get_sections(
        eln_config.db_engine, page_id
        )
    archive_path = eln_config.get_path('data_dir')
    page_path = os.path.join(archive_path, page.fpath)
    gitcall = githandling.GitCall(page_path)

    # Create .lock file and add/commit every html file in the history path
    # and the lock file itself.
    with open(os.path.join(page_path, '.lock'), 'w') as lock_file:
        pass
    gitcall.add('.lock').communicate()
    
    for page_section in page_sections:
        gitcall.add(page_section.fname).communicate()

    commit_message = githandling.get_commit_message(
        gitcall, heading='Page submitted.'
        )
    gitcall.commit(
        commit_message, timestamp,
        page.user_username, page.user_email
        ).communicate()
    # Update database:
    # set submitted=True and update update_time.
    db_modules.PageAccess(eln_config.db_engine).submit_page(
        page_id, submit_time=timestamp
        )
    
