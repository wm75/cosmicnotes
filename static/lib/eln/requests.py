import save
import export
import experiment
import db_modules


def handle_request (eln_config, request):
    request_type =  request['request_type']
    request_params = request['params']
    request_data = request['data'] or None
    if 'timestamp' not in request_params:
        request_params['timestamp'] = experiment.timestamp()

    if request_type == 'save_section':
        return handle_save_request(eln_config, request_params, request_data)
    elif request_type == 'submit':
        return handle_submit_request(eln_config, request_params)
    elif request_type == 'export':
        return handle_export_request(eln_config, request_params, request_data)
    elif request_type == 'update_projects':
        return handle_update_projects_request(eln_config, request_params)
    else:
        raise TypeError('Unknown request type: ' + request_type)


def handle_save_request (eln_config, request, data):
    page_id = request['page_id']
    section_no = request['section_no']
    is_erratum = request.get('is_erratum', False)
    editor_data = data or ''
    timestamp = request['timestamp']
    section = db_modules.get_section(
        eln_config.db_engine, page_id, section_no
        )
    if not section:
        section = experiment.Section(
            id=None, page_id=page_id, section_no=section_no,
            erratum = is_erratum
            )
    saved_section = save.save_section(
        eln_config, section, editor_data, timestamp
        )
    return saved_section.__dict__


def handle_submit_request (eln_config, request):
    page_id = request['page_id']
    timestamp = request['timestamp']
    return save.submit_page(eln_config, page_id, timestamp)


def handle_export_request (eln_config, request):
    page_id = request['page_id']
    page_associated_history_name = request['history_name']
    page_associated_history_content = request['history_content']
    return export.export(
        eln_config,
        page_id,
        page_associated_history_name,
        page_associated_history_content
        )


def handle_update_projects_request (eln_config, request):
    access_method = request['action']
    access_method_args = []
    for param in ('project_id', 'project_name', 'page_id'):
        if param in request:
            access_method_args.append(request[param])
    p = db_modules.ProjectAccess(eln_config.db_engine)
    return getattr(p, access_method)(*access_method_args)
    
