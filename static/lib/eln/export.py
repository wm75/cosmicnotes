# Warning:
# export functionality is work in progress
# the code in this module is currently not used in CosmicNotes, partially
# outdated and non-functional.

import json
import os
import shutil
import zipfile

import eln_util
import experiment
import db_modules
import githandling


def rm_file_extension (name, extension) :
    """ Removes the file extension, when existing and identical to the received extension.
    """
    file_name,file_extension = os.path.splitext(name)
    if file_extension:
        if extension == file_extension[1:].lower():
            return file_name
        else:
            return name
    else:
        return name


def get_file_paths (path, suffix):
    """ Checks path for html files and returns a sorted list of names."""
    file_paths = []
    if os.path.isdir(path):
        for file in os.listdir(path):
            if file.endswith("." + suffix):
                file_paths.append(os.path.join(path, file))
        file_paths.sort()
    return file_paths
  
  
def generate_dataset_legend (dataset_list):
    dataset_legend = '\n'.join("""\
<tr>
<td><a href="./{0.export_path}" title="{0.name}">{0.name}</a></td>
</tr>"""
        .format(dataset)
        for dataset in dataset_list
    )

    dataset_legend_template = """\
<div class='seperator'>Dataset Overview</div>
<div class ='annotation_overview'>
    <table class='dataset_table'>
        {dataset_legend}
    </table>
</div>"""
    dataset_legend_template = unicode(dataset_legend_template)
    return dataset_legend_template.format(dataset_legend = dataset_legend)


def generate_entry_html (entry, dropdown_options, i=0):
    if entry.is_modified():
        last_modified_template = """
        <font color="red">Last Modify: {last_modify_date}</font> 
        """
        last_modified_template = unicode(last_modified_template)
        last_modify = last_modified_template.format(last_modify_date=entry.date_last_modified)
        
        dd_template = """
        <div class='dropdown'>
            <form>
                <select onchange='ChangeContent({i})' id='dropdown_{i}'>
                    ${dropdown_option_tag}
                </select>
            </form>
        </div>
        """
        dd_template = unicode(dd_template)
        dropdown_box = dd_template.format(
            dropdown_option_tag=dropdown_options,
            i=i
        )
    else:
        last_modify = ''
        dropdown_box = ''
    
    template = """
    <div class='entry'>
        <div class= 'entry_header'>
            <div class='date'>
                Creation Date: {creation_date} <br>
                {last_modify}
            </div>            
            {dropdown_box}
        </div>
        <div class= 'entry_content_box'> 
            <div class= 'entry_content' id='content_{i}'>
                {content}
            </div>
        </div>
    </div>
    """    
    
    template = unicode(template).format(
        creation_date=entry.date_created,
        content=entry.content,
        last_modify=last_modify,
        dropdown_box=dropdown_box,
        i=i,
    )
    return template


def notebook_template_html (history_title, export_css, entry_html, old_versions_json, dataset_legend_html):
    template = """
    <!DOCTYPE HTML>
    <html>
        <head> 
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <style>
                {export_css}
            </style>
        </head>        
        <body>            
            <script>
            var old_versions = {old_versions_json};            

            function ChangeContent(number) {{            
                var content_box = document.getElementById("content_"+number);
                var old_version = old_versions[number];
                var dd = document.getElementById("dropdown_"+number);
                var dd_value = dd.options[dd.selectedIndex].value;
                content_box.innerHTML = old_version[dd_value];
            }}            
            </script>
            <div class='title'> {history_title} </div>
            {entry_html}   
            {dataset_legend_html}
        </body>
    </html>
    """
    template = unicode(template).format(
        history_title=history_title,
        export_css=export_css,
        entry_html=entry_html,
        old_versions_json=old_versions_json,
        dataset_legend_html=dataset_legend_html,
    )       
    return template


class ExportDataset (object):
    def __init__(self, dataset_dict):
        
        self.url = dataset_dict['url']
        self.extension = dataset_dict['extension']
        self.name = self.rm_file_extension(
            dataset_dict['name'], dataset_dict['extension']
        )
        self.id = dataset_dict['id']
        self.export_path = 'datasets/{0.name}.{0.extension}'.format(self)
        
    def rm_file_extension (self, name, extension) :
        """ Removes the file extension, when existing and identical to the received extension.
        """
        file_name,file_extension = os.path.splitext(name)
        if file_extension:
            if extension.lower() == file_extension[1:].lower():
                return file_name
            else:
                return name
        else:
            return name


def export (eln_config, enc_hist_id, history_content, trans):
    hist_id = str(trans.security.decode_id(enc_hist_id))
    hist_name = db_modules.get_hist_name(eln_config.db_engine, hist_id)
    history_dict = json.loads(history_content)
    dataset_list = [ExportDataset(dataset) for dataset in history_dict
                    if not dataset['deleted'] and not dataset['purged']]

    export_path = eln_config.get_path('export_dir')
    export_full_path = os.path.join(export_path, hist_name + '.zip')
    # save datasets in zipfile
    with zipfile.ZipFile(
        export_full_path, 'w', compression=zipfile.ZIP_DEFLATED
    ) as zf:
        archive_path = eln_config.get_path('data_dir')
        hist_path = os.path.join(archive_path, hist_id)
              
        # save old versions
        # todo: USERs choice to save old versions
        gitcall = githandling.GitCall(hist_path)
        entry_paths = get_file_paths(hist_path, 'html')
        temp_path = os.path.join(hist_path, 'temp')
        old_versions= []
        dropdown_options= []
        try:                    
            # Create a new dir "temp" and clones the repository into it.
            os.mkdir(temp_path)
            gitcall.clone(temp_path).communicate()    
            gitcall_temp = githandling.GitCall(temp_path)        
             
            for entry_path in entry_paths:
                commit_date, commit_hash = githandling.commit_msg_readout(
                    gitcall, entry_path
                )
                if commit_date:
                    temp_file_path = os.path.join(
                        temp_path, os.path.basename(entry_path)
                    )
                    old_ver_html_content, old_ver_rst_content = githandling.checkout_old_version(gitcall_temp, commit_hash, temp_file_path)
                    # Validate the selected file for a non-commited change and
                    # append it to the date and content list.
                    commit_date, old_ver_html_content, old_ver_rst_content = githandling.check_current_working_file(gitcall, commit_date, old_ver_html_content, old_ver_rst_content, entry_path)
                    # Create an insertable option tag for the js dropdown menus
                    # containing the commit dates.
                    dropdown_option_tag = eln_util.list_to_jsoption_tag(range(0,len(commit_date)), commit_date)
                    old_versions.append(old_ver_html_content)
                    dropdown_options.append(dropdown_option_tag)
                else:
                    old_versions.append('')
                    dropdown_options.append('')
            # Convert old_versions to json format
            old_versions_json = json.dumps(dict(enumerate(old_versions)))    
        finally:
            if os.path.isdir(temp_path):
                shutil.rmtree(temp_path)

        # save notebook.html
        file_org = sorted(experiment.get_sections(hist_path),
                          key=lambda x: x.section_no)
        entries_html = ''.join(
            generate_entry_html(entry, dropdown_options[i], str(i))
            for i, entry in enumerate(file_org)
        )        
        css_template = os.path.join(eln_config.get_path('stylesheets'), 'export.css')
        with open(css_template, 'r') as css:
            export_css = css.read()       
        dataset_legend_html = generate_dataset_legend(dataset_list)
        notebook_page = notebook_template_html(
            hist_name, export_css, entries_html, old_versions_json, dataset_legend_html
        )
        zf.writestr('notebook.html', notebook_page.encode('utf-8'))
