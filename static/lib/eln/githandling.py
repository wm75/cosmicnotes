import subprocess
import os
import tempfile
import shutil
import codecs

from contextlib import contextmanager

import pypandoc


class GitCall (object):
    """ This class eneables git commandline calls via a subprocess.Popen object.
    While initializing an instance, the path of the repository have to be passed.
    The output of all methods are Popen objects. To interact with them you have to 
    use the communicate() method of the subprocess modul.
    """
    
    def __init__ (self, cwd):
        """Initialize a new git repository in cwd.

        Creates the top-level folder if necessary.
        """

        self.cwd = cwd
        if not os.path.isdir(cwd):
            os.mkdir(cwd)
        self.call('init').communicate()

    @contextmanager    
    def tmp_clone (self, root_path):
        tmp_dir = tempfile.mkdtemp(dir=root_path)
        try:
            tmp_repo = os.path.join(
                tmp_dir, os.path.basename(self.cwd)
                )
            self.clone(tmp_repo).communicate()
            yield tmp_repo
        finally:
            shutil.rmtree(tmp_dir, ignore_errors=True)
            
    def extract_commit_history (self, filename, report_pending=False):
        """Parse the commit history of a single file in the repository.

        Returns a list of [commit_date, commit_hash] pairs sorted by date
        (latest commit first). If report_pending is True and there are
        uncommitted changes to the file, the first element of the list will be
        a ['current', '--'] pair indicating the presence of the uncommitted
        change.
        """

        commits = []
        if report_pending:
            stdout, stderr = self.diff_name_only(filename).communicate()
            if stdout:
                commits.append(['current', '--'])
        stdout_log, stderr_log = self.call(
            'log', '--pretty=format:%ai#%H', filename
            ).communicate()
        for line in stdout_log.split('\n'):
            if line:
                commits.append(line.split('#'))
        return commits

    def diff_name_only (self, filename):
        """ Executes a git diff in the current repository.
        When there are diffs in the repostory, the output are only the file names of the
        changed files.
        """
        return self.call("diff", "--name-only", filename)

    def commit (self, commit_message,
                author_date = None, author_name = None, author_email = None):
        """ Executes a git commit with the given commit message in the current repository.
        """
        
        call_args = ['commit', '-m', commit_message]
        if author_date:
            call_args += ['--date', str(author_date)]
        if author_name and author_email:
            call_args += ['--author', '{0} <{1}>'
                          .format(author_name, author_email)]
        return self.call(*call_args)

    def status_porcelain (self):
        """ Outputs the repository status in the porcelein format.
        eg. "M  example.html
             A  example2.html"
        """
        return self.call('status', '--porcelain')

    def log_pretty (self, file_path = None):
        """ Lists the commits of the current repository of a specific file.
        When no file path is given it outputs only the date of the latest commit in the 
        whole repository.
        The Output is formatted like: first_line(date)#commit_hash.
        """
        # %s: subject, first line of the commit message. #: Seperator. %H: commit hash.
        if file_path:
            return self.call("log", "--pretty=format:%s#%H", file_path)
        else:
            return self.call("log", "--pretty=format:%ci", "-n", "1")

    def checkout (self, commit_hash, file_path):
        """ Restores the file to the recieved commit state.
        """
        return self.call("checkout", commit_hash, file_path)

    def clone (self, clone_target_path):
        """ Clones the current repository to the target path.
        When the target_path doesn't exits, git creats the directory.
        """
        return self.call("clone", ".", clone_target_path)    

    def add (self, file_path):
        """ Adds the received file to git repository HEAD.
        """
        return self.call("add", file_path)       

    def config (self, config_type, entry):
        """ Changes the configuration of the git repository.
        The first argument is the configuration type, which should be changed.
        The second argument is the actual configuration value, which is used to replace 
        the current configuration.
        """
        return self.call("config", config_type, entry)  

    def call (self, *args, **kwargs):
        """ Creats a Popen object via a subprocess.Popen call.
        Changes the cwd to the class given cwd and restores the original cwd after 
        generating the Popen object. Additionally to the received arguments via the
        method call, some arguments and keyword arguments are added, which are neccessary
        for the git subprocess call. 'git' is added to the first position of the command
        line call. The following kewords are also added in this method.
        "stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines=True" 
        """

        call_args = ['git'] + list(args)
        call_env = os.environ.copy()
        call_env['GIT_COMMITTER_NAME'] = 'cosmicnotes'
        call_env['GIT_COMMITTER_EMAIL'] = 'cosmicnotes@galaxy.org'
        call_kwargs = dict(kwargs,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           universal_newlines=True,
                           cwd=self.cwd,
                           env=call_env
                          )
        popen_obj = subprocess.Popen(call_args, **call_kwargs)
        return popen_obj
    

def get_commit_message (gitcall_obj, heading = None):
    """Creats a commit message out of the information of git status and appends a
    last modify date. The cwd have to be set to the history! 
    input: datetime_obj, last modify date of the notebook. 
    output: string, formated commit message
    """

    stdout_status, stdder_status = gitcall_obj.status_porcelain().communicate()
    # --porcelain returns '\n'-separated output. Last element is \n.
    message_parts = []
    if heading:
        message_parts.append(heading + '\n')
    if stdout_status:
        status_list = stdout_status.rstrip("\n").split("\n")
        message_parts = []
        if heading:
            message_parts.append(heading + '\n')
        for record in status_list:
            if record[0] == "M":
                message_parts.append("modified  " + record[3:])
            if record[0] == "A":
                message_parts.append("added     " + record[3:])
    commit_message = "\n".join(message_parts)
    return commit_message
