from sqlalchemy import MetaData, Table, Column
from sqlalchemy import Integer, String, DateTime, Boolean
from sqlalchemy.sql import and_, or_, not_

import sqlalchemy

import experiment


# Define the Database Schema
METADATA = MetaData()

PAGES = Table('cosmicnotes_page', METADATA,
    Column('id', Integer, primary_key=True),
    Column('user_id', Integer),
    Column('create_time', DateTime),
    Column('update_time', DateTime),
    Column('commit_time', DateTime),
    Column('submitted', Boolean),
)

PROJECTS = Table('cosmicnotes_project', METADATA,
    Column('id', Integer, primary_key=True),
    Column('name', String(255)),
)

PROJECTS_PAGES_ASSOC = Table(
    'cosmicnotes_project_page_association', METADATA,
    Column('id', Integer, primary_key=True),
    Column('project_id', Integer),
    Column('page_id', Integer),
)

USERS = Table('cosmicnotes_user', METADATA,
    Column('id', Integer, primary_key=True),
    Column('username', String(255)),
    Column('email', String(255)),
)

SECTIONS = Table('cosmicnotes_section', METADATA,
    Column('id', Integer, primary_key=True),
    Column('page_id', Integer),
    Column('section_no', Integer),
    Column('erratum', Boolean),
    Column('create_time', DateTime),
    Column('update_time', DateTime),
    Column('commit_time', DateTime),
    Column('header', String(255)),
    Column('content', String(21000)),
)


class SectionAccess (object):
    def __init__ (self, engine):
        self.engine = engine

    def insert_section (self, section):
        connection = self.engine.connect()
        with connection.begin() as trans:
            result = connection.execute(
                SECTIONS.insert().values(
                    page_id=section.page_id,
                    section_no=section.section_no,
                    erratum=section.is_erratum,
                    create_time=section.create_time,
                    update_time=section.update_time,
                    commit_time=section.commit_time,
                    content=section.content
                    )
                )
            
            valid_timestamps = {}
            if section.commit_time is not None:
                valid_timestamps['commit_time'] = section.commit_time
            if section.update_time is not None:
                valid_timestamps['update_time'] = section.update_time
            
            result = connection.execute(
                PAGES.update()
                .where(PAGES.c.id == section.page_id)
                .values(**valid_timestamps)
                )

        
    def update_section (self, section):
        connection = self.engine.connect()
        with connection.begin() as trans:
            valid_timestamps = {}
            if section.commit_time is not None:
                valid_timestamps['commit_time'] = section.commit_time
            if section.update_time is not None:
                valid_timestamps['update_time'] = section.update_time

            result = connection.execute(
                SECTIONS.update()
                .where(and_(SECTIONS.c.page_id == section.page_id,
                            SECTIONS.c.section_no == section.section_no))
                .values(content=section.content,
                        **valid_timestamps)
                )

            result = connection.execute(
                PAGES.update()
                .where(PAGES.c.id == section.page_id)
                .values(**valid_timestamps)
                )
        connection.close()


class PageAccess (object):
    def __init__ (self, engine):
        self.engine = engine

    def insert_page (self,
                     id,
                     create_time, update_time,
                     user_id,
                     user_username=None, user_email=None,
                     submitted = False):
        """Insert a new page into the database.

        If the new page is owned by a user that is not yet in the database,
        a new user record is also generated. This feature requires that a
        username and an email are provided (if either is missing, the operation
        will be rolled back and aborted.
        """

        connection = self.engine.connect()
        with connection.begin() as trans:
            result = connection.execute(
                PAGES.insert().values(
                    id=id,
                    user_id=user_id,
                    create_time=create_time,
                    update_time=update_time,
                    submitted=submitted
                    )
                )
            is_existing_user = get_users_from_ids(
                connection, [user_id]
                )
            if not is_existing_user:
                if user_username is None or user_email is None:
                    raise ValueError(
                        'The page you are trying to insert is owned by a user '
                        'who is not recorded in the database yet.\n'
                        'To add the user to the database you need to provide '
                        'username and email to this function in addition to '
                        'the user id.'
                        )
                result = connection.execute(
                    USERS.insert().values(
                        id=user_id,
                        username=user_username,
                        email=user_email
                        )
                    )
        connection.close()

    def update_page (self, page_id,
                     update_time):
        connection = self.engine.connect()
        with connection.begin() as trans:
            result = connection.execute(
                PAGES.update()
                .where(PAGES.c.id == page_id)
                .values(update_time=update_time)
                )

    def submit_page (self, page_id, submit_time):
        connection = self.engine.connect()
        with connection.begin() as trans:
            result = connection.execute(
                PAGES.update()
                .where(PAGES.c.id == page_id)
                .values(commit_time=submit_time,
                        submitted=True)
                )
            result = connection.execute(
                SECTIONS.update()
                .where(SECTIONS.c.page_id == page_id)
                .values(commit_time=submit_time)
                )


class ProjectAccess (object):
    def __init__ (self, engine):
        self.engine = engine

    def insert_association (self, project_id, page_id):
        connection = self.engine.connect()
        with connection.begin() as trans:
            result = connection.execute(
                PROJECTS_PAGES_ASSOC.insert().values(
                    project_id = project_id,
                    page_id = page_id
                    )
                )
        connection.close()

    def delete_association (self, project_id, page_id):
        connection = self.engine.connect()
        with connection.begin() as trans:
            result = connection.execute(
                PROJECTS_PAGES_ASSOC.delete().where(
                    and_(
                        PROJECTS_PAGES_ASSOC.c.page_id == page_id,
                        PROJECTS_PAGES_ASSOC.c.project_id == project_id,
                        )
                    )
                )
        connection.close()

    def insert_project (self, project_name):
        connection = self.engine.connect()
        with connection.begin() as trans:
            name_exists = connection.execute(PROJECTS.select()
                .where(PROJECTS.c.name == project_name)            
            ).fetchone()
            if name_exists:
                new_project_id = None
            else:
                result = connection.execute(
                    PROJECTS.insert().values(
                        name = project_name
                        )
                    )
                new_project_id = result.inserted_primary_key[0]
        connection.close()
        return experiment.ExpID(new_project_id)


def collect_filtered_pages_metadata (conn,
                                     query_author,
                                     query_project,
                                     query_text,
                                     status_filter):
    """Generate a list of metadata of all pages matching a user query.

    Queries can be any combination of author, project, page text content and
    page status (submitted or not).

    The returned metadata list can form the basis of a multipanel view.
    """

    pages = []
    conditions = []
    connection = conn.connect()
    with connection.begin() as trans:
        if query_author:
            _ = connection.execute(
                USERS.select()
                .where(USERS.c.username == query_author)
                )
            query_author_id = _.fetchone()['id']
            conditions.append(PAGES.c.user_id == query_author_id)
        if query_project:
            query_page_ids = get_associated_page_ids(connection, query_project)
            conditions.append(PAGES.c.id.in_(query_page_ids))
        if status_filter == 'only_submitted':
            conditions.append(PAGES.c.submitted == True)
        elif status_filter == 'only_non_submitted':
            conditions.append(PAGES.c.submitted == False)

        if conditions:
            matched_pages = connection.execute(
                PAGES.select().where(and_(*conditions))
                )
        else:
            matched_pages = connection.execute(PAGES.select())
            
        for row in matched_pages:
            page_meta  = {
                'id': row['id'],
                'user_id' :  row['user_id'],
                'create_time': row['create_time'],
                'update_time': row['update_time'],
                'model_class': 'History',
                'submitted': row['submitted']
            }
            pages.append(page_meta)

        if pages and query_text:
            page_ids = {meta['id'] for meta in pages}
            result = connection.execute(
                SECTIONS.select(SECTIONS.c.content.match(query_text))
                .where(SECTIONS.c.page_id.in_(page_ids))
                )
            lb_search_hits = [row['page_id'] for row in result]
            # Filter text search
            pages = [page for page in pages if page['id'] in lb_search_hits]
        else:
            lb_search_hits = []

    connection.close()
    return pages, lb_search_hits


def get_section (conn, page_id, section_no):
    connection = conn.connect()
    with connection.begin() as trans:
        result = connection.execute(SECTIONS.select()
            .where(and_(SECTIONS.c.page_id == page_id,
                        SECTIONS.c.section_no == section_no))            
        ).fetchone()
    connection.close()

    if result:
        return experiment.Section(**result)


def get_sections (conn, page_id):
    connection = conn.connect()
    with connection.begin() as trans:
        results = connection.execute(SECTIONS.select()
            .where(SECTIONS.c.page_id == page_id)            
        )
    sections = [experiment.Section(**section_data) for section_data in results]
    connection.close()

    sections.sort(key=lambda s: s.section_no)
    return sections
    

def get_associated_project_ids (conn, page_ids=None):
    """Return the ids of all projects which the given pages are associated with."""

    connection = conn.connect()
    with connection.begin() as trans:
        if page_ids:
            matched_assocs = connection.execute(
                PROJECTS_PAGES_ASSOC.select()
                .where(PROJECTS_PAGES_ASSOC.c.page_id.in_(page_ids))
                )
            project_ids = {
                row[PROJECTS_PAGES_ASSOC.c.project_id]
                for row in matched_assocs
                }
        else:
            matched_assocs = connection.execute(PROJECTS_PAGES_ASSOC.select())
            project_ids = {
                row[PROJECTS_PAGES_ASSOC.c.project_id]
                for row in matched_assocs
                }
    connection.close()
    return project_ids


def get_associated_page_ids (conn, project_id):
    """Return the ids of all pages which are associated with the given project."""

    connection = conn.connect()
    with connection.begin() as trans:
        result_assoc = connection.execute(
            PROJECTS_PAGES_ASSOC.select()
            .where(PROJECTS_PAGES_ASSOC.c.project_id == project_id)
        )
        page_ids = [row['page_id'] for row in result_assoc]
    connection.close()
    return page_ids


def get_projects_from_ids (conn, project_ids = None):
    connection = conn.connect()
    with connection.begin() as trans:
        if project_ids is None:
            results = connection.execute(PROJECTS.select())
        else:
            results = connection.execute(
                PROJECTS.select()
                .where(PROJECTS.c.id.in_(project_ids))
                )
        projects = [dict(row) for row in results]
    connection.close()
    return projects


def get_pages_from_ids (conn, page_ids = None):
    connection = conn.connect()
    with connection.begin() as trans:
        if page_ids is None:
            results = connection.execute(PAGES.select())
        else:
            results = connection.execute(
                PAGES.select()
                .where(PAGES.c.id.in_(page_ids))
                )
        pages = [dict(row) for row in results]

    users = get_users_from_ids(connection, {p['user_id'] for p in pages})

    connection.close()

    user_mapping = {u['id']: [u['username'], u['email']] for u in users}
    for page in pages:
        page['user_username'], page['user_email'] = user_mapping[page['user_id']]
        
    return [experiment.Page(**page_data) for page_data in pages]


def get_users_from_ids (conn, user_ids = None):
    connection = conn.connect()
    with connection.begin() as trans:
        if user_ids is None:
            results = connection.execute(USERS.select())
        else:
            results = connection.execute(
                USERS.select()
                .where(USERS.c.id.in_(user_ids))
                )
        users = [dict(row) for row in results]
    connection.close()
    return users
    

def create_tables(engine):
    """Creates the Tables defined in the module-level METADATA in the database.

    Skips Tables pre-existing in the database (accessed via engine).
    """

    METADATA.create_all(engine)


def make_url (db_conn_str):
    return sqlalchemy.engine.url.make_url(db_conn_str)


def create_engine (db_url):
    return sqlalchemy.create_engine(db_url)
