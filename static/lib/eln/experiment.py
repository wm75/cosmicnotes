import os
import codecs

from datetime import datetime

import pypandoc


def timestamp ():
    datetime_str = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    return datetime.strptime(datetime_str,"%Y-%m-%d %H:%M:%S")


class Section (object):
    def __init__ (self, id, page_id, section_no, erratum,
                  create_time = None, update_time = None, commit_time = None,
                  header = None, content = None, html_content = None):
        self.id = ExpID(id) if id is not None else id
        self.page_id = ExpID(page_id)
        self.section_no = section_no
        self.fname = get_section_fname_from_no(page_id, section_no)
        self.is_erratum = erratum
        self.create_time = create_time
        self.update_time = update_time
        self.commit_time = commit_time
        self.header = header
        self.content = content
        self.html_content = html_content
        
    def load_rst_content (self, archive_path):
        if self.content is None:
            self.load_html_content(archive_path)
            self.content = pypandoc.convert(
                self.html_content, 'rst', format='html'
                )

    def load_html_content (self, archive_path):
        if self.html_content is None:
            page_path = os.path.join(
                archive_path, get_page_path_from_id(self.page_id)
                )
            section_path = os.path.join(page_path, self.fname)
            with codecs.open(section_path, 'r', encoding='utf-8') as source:
                header = source.readline().strip('\n')
                # CKEditor adds \r\n to the end of documents,
                # which would cause an extra newline in the rendered mako code!
                # Since we are dealing with html format, we can simply remove
                # all linebreak characters.
                self.html_content = source.read().replace("\n", "").replace("\r", "")
    
    def is_modified (self):
        return self.create_time.date() != self.update_time.date()


class Page (object):
    def __init__ (self, id, user_id, user_username, user_email,
                  create_time, update_time, commit_time,
                  submitted):
        self.id = ExpID(id)
        self.fpath = get_page_path_from_id(id)
        self.user_id = ExpID(user_id)
        self.user_username = user_username
        self.user_email = user_email
        self.create_time = create_time
        self.update_time = update_time
        self.commit_time = commit_time
        self.submitted = submitted


class ExpID (int):
    pass


def get_section_fname_from_no (page_id, section_no):
    """Return the name of the file corresponding to a given ELN section.

    The section is identified by its page and its number within that page.
    """
    
    return 'section_{0}.html'.format(section_no)


def get_page_path_from_id (page_id):
    """Return the path to the archive of the sections of a given ELN page.

    The path is relative to ElnConfig().get_path('data_dir').
    """
    
    return str(page_id)
