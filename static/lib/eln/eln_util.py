import os
import posixpath
import shutil
import ConfigParser
import cgi

import db_modules


class ElnConfig (ConfigParser.SafeConfigParser):
    def __init__(self, app_dir, url_base):
        ConfigParser.SafeConfigParser.__init__(self)
        self.app_dir = app_dir
        self.url_base = url_base

        # read config files
        # (creating eln config file from template if necessary
        with open(os.path.join(app_dir, 'component_paths.cfg')) as comp_cfg:
            self.readfp(comp_cfg)
        eln_cfg_file = os.path.join(app_dir, self.get('Paths', 'config_file'))
        try:
            with open(eln_cfg_file) as eln_cfg:
                self.readfp(eln_cfg)
        except IOError:
            shutil.copyfile(eln_cfg_file + '.sample', eln_cfg_file)
            with open(eln_cfg_file) as eln_cfg:
                self.readfp(eln_cfg)

        # cofigure settings
        if self.has_option('Database', 'data_dir'):
            self.set('Paths', 'data_dir', self.get('Database', 'data_dir'))
        else:
            self.set('Paths', 'data_dir', self.get('Paths', 'data_default'))
        data_dir = os.path.join(app_dir, self.get('Paths', 'data_dir'))
        if not os.path.isdir(data_dir):
            os.mkdir(data_dir)
        if self.has_option('Export', 'export_path'):
            self.set(
                'Paths', 'export_dir', self.get('Export', 'export_path')
            )

        # initiate database access
        self.db_engine = self.init_database()
        
    def export_enabled(self):
        # !! disable export in the current version !!
        # if self.has_option('Export', 'enable_export'):
        #    return self.getboolean('Export', 'enable_export')
        # else:
        #    return False

        return False
      
    def get_path (self, component):
        if not self.has_option('Paths', component):
            raise LookupError(component + ' is not configured')
        path = os.path.normpath(
            os.path.join(self.app_dir, self.get('Paths', component))
        )
        if not os.path.isdir(path):
            raise IOError(path + ' is not a valid directory')
        return path

    def get_url (self, component):
        for section in ('URLPaths', 'DEFAULT'):
            if self.has_option(section, component):
                break
        else:
            raise LookupError(component + ' is not configured')
        urlpath = posixpath.join(
            self.url_base,
            posixpath.normpath(self.get(section, component))
        )
        return urlpath
        
    def get_autosave_interval (self):
        if not self.has_option('Editor', 'autosave_timer'):
            raise LookupError(('autosave_timer is not configurated'))
        interval = self.get('Editor', 'autosave_timer')
        try:
            int_interval = int(interval)
        except:
            raise ValueError ('autosave_timer has to be a natural number')
        return interval
        
    def init_database (self):
        if not self.has_option('Database', 'database_connection_string'):
            raise LookupError(('database_connection_string is not configured'))
        # Generate an SQLAlchemy url instance from the connection string and,
        # for an sqlite database, normalize its database attribute to the app
        # dir. This allows relative sqlite database paths to be provided in the
        # config file.
        
        db_url = db_modules.make_url(
            self.get('Database', 'database_connection_string')
            )
        if db_url.get_dialect().name =='sqlite':
            db_url.database = os.path.normpath(
                os.path.join(self.app_dir, db_url.database)
        )
        db_engine = db_modules.create_engine(db_url)
        db_modules.create_tables(db_engine)
        return db_engine
        
    def db_is_postgres(self):
        return self.db_engine.url.get_dialect().name == 'postgresql'
        
    def db_is_mysql(self):
        return self.db_engine.url.get_dialect().name == 'mysql'


def build_select_options (option_value_pairs):
    return [
        u'<option value="{value}">{option}</option>'.format(
            option=cgi.escape(o), value=v
            )
        for o, v in option_value_pairs
        ]
