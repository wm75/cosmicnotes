This is a fork of the [original CosmicNotes repo of Fabrice
Hess](https://bitbucket.org/Fabrice_Hess/cosmicnotes). Active development of
CosmicNotes is going to happen mostly here and updates to the original repo
will be merged regularly.

### **CosmicNotes:** *Write your daily lab notebook entries within Galaxy.*
CosmicNotes is an electronic lab notebook plugin for the [Galaxy bioinformatics
platform](https://usegalaxy.org). CosmicNotes uses Galaxy's visualizations
framework to enable the creation of richly formatted lab book entries
accompanying Galaxy histories through a web editor.
CosmicNotes lab book entries are version controlled (using Git). Together with
an integrated diff viewer, this allows for fully editable lab book entries
until final submission of a project, while supporting full tracking of all
changes.
CosmicNotes labbook entries can be grouped into projects and can be filtered by
authors and names.

### Installation instructions: ###

To install CosmicNotes, it is sufficient to git clone the repository directly
into the vizualisations directory of your Galaxy instance
(galaxy/config/plugins/visualizations), to add the new visualization
to Galaxy's additional_template_paths.xml file and to restart your Galaxy
server. This will run CosmicNotes with default settings (SQLite database, all
labbook data stored inside the cosmicnotes folder. To configure CosmicNotes
manually, copy the included config/eln.cfg.sample file to config/eln.cfg and
modify it according to your needs. To use a postgresql database instead of the
preconfigured SQLite database, you need to create the database and provide
its connection URL in the config file.

### Dependencies: ###
[Pandoc](http://pandoc.org/)
[Git](https://git-scm.com/)

### Updating CosmicNotes: ###
just git pull
